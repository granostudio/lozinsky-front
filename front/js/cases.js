$('.brand-slider').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    appendArrows: '.brand-slider-btn-block',
    prevArrow: '<button type="button" class="slick-prev slick-arrow-btn"></button>',
    nextArrow: '<button type="button" class="slick-next slick-arrow-btn"></button>',
    responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 5,
            lidesToScroll: 1,
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          }
        }
      ]
  });