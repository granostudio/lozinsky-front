// SLIDER TOP
var $status = $('.sliderTopInfo');
var $slickElement = $('.sliderTopContent');
var $dotsBlock = $('.slick-dots');
var $tempoTransicao = 2000;
var $temConvert = $tempoTransicao / 1000.0;
$slickElement.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
    //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
    var i = (currentSlide ? currentSlide : 0) + 1;
    // $status.text(i + '/' + slick.slideCount);
    $('.sliderTopControl .slick-active').append('<div class="timeBar" style="width:0px;transition: width ' + $temConvert +'s linear 0s"></div>');
    $('.sliderTopControl .slick-active').append('<div class="timeBarBg"></div>');
     setTimeout(
        function() 
        {
            $('.timeBar').css('width','45px');
            setTimeout(
                function() 
                {
                    $('.sliderTopControl li:not(.slick-active) .timeBar ').remove();
                    $('.sliderTopControl li:not(.slick-active) .timeBarBg').remove();

                }, 1 );
        }, 0);
    
});

$('.sliderTop .sliderTopContent').slick({
    appendArrows: '.sliderTopControl .arrowBlock',
    appendDots: '.sliderTopControl .sliderTopInfo',
    dots: true,
    autoplay: true,
    nextArrow: '<button type="button" class="slick-arrow-btn slick-next"><img src="images/banner-topo/right-arrow.png"></button>',
    prevArrow: '<button type="button" class="slick-arrow-btn slick-prev"><img src="images/banner-topo/left-arrow.png"></button>',
    // autoplay: true,
    autoplaySpeed: $tempoTransicao,
    customPaging : function(slider, i) {
        var thumb = $(slider.$slides[i]).data();
        if (i < 10) {
            return '<a>'+ '0' + (i+1)+'</a>';
        } 
        else {
            return '<a>'+(i+1)+'</a>';
        }
    }
})


// SLIDER DE CASES
$('.sliderCasesContent').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    arrows: false,
    fade: false,
    asNavFor: '.sliderCasesLogo'
});
$('.sliderCasesLogo').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    asNavFor: '.sliderCasesContent',
    dots: false,
    nextArrow: '<button type="button" class="slick-next"><img src="images/right-arrow-btn.png"></button>',
    prevArrow: '<button type="button" class="slick-prev"><img src="images/left-arrow-btn.png"></button>',
    // centerMode: true,
    focusOnSelect: true,
    responsive: [
        {
        breakpoint: 500,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
        }
        },
    ]
});
// FIM SLIDER DE CASES

var videoCover = $('.video-cover');
videoCover.click(function(){
    $(this).hide();
})