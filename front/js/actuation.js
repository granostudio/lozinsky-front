function marginFunc() {
    // PEGA MARGIN ESQUERDA DO CONTAINER
    var container = $('.wrapperContent');
    var containerMarginLeft = parseInt(container.css('margin-left'));
    console.log(containerMarginLeft);
    
    // COLOCA A MARGIN DO CONTAINER NO BLOCO
    var actuationTopInfoContent = $('.actuation-top-info-content');
    actuationTopInfoContent.css({'margin-left':containerMarginLeft});
}
$( document ).ready(function() {
    marginFunc();
});
$(window).resize(function(){
    marginFunc();
});

var circleTitleContent = $('.circle-title-content');
var circleTextContent = $('.circle-text-content');

$('.actuation-top-item').click(function(){
    let newValueTitle = $(this).children('.actuation-top-item-title').text();
    circleTitleContent.text(newValueTitle);

    let newValueText = $(this).children('.actuation-top-item-text').text();
    circleTextContent.text(newValueText);
});



