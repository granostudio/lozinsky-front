<?php
/**
 * The template for displaying 404 pages (Not Found)
 */
get_header();
?>
<div class="breadcumbs">
        <div class="wrapperContent">
        <?php
                        if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb( '<p id="breadcumbsText">','</p>' );
                        }
                        ?>
        </div>
</div>
<p class="not-found"><img src="http://lozinsky.hospedagemdesites.ws/wp-content/themes/webexpresso/images/404.png" alt=""></p>
<?php
get_footer();
?>