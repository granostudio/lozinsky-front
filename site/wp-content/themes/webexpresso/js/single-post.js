$(document).ready(function(){
    var podcastTitle = $('.podcast-block .podcast-title');
    if($('.podcast-block').length > 0){ 
        function mudaTexto() {
            if ($(window).width() <= 800) {
                podcastTitle.text("Agora você também pode ouvir o conteúdo desta página no player abaixo");
            }
            else {
                podcastTitle.text("Agora você também pode ouvir o conteúdo desta página no player ao lado");
            }
        }
        mudaTexto();
        $(window).resize(function(){
            mudaTexto();
        });
    };
});