$(document).ready(function(){
    // grafico 1
    $('.visao .btn').on( "mouseover", function(){
        $(this).find(".tooltip").addClass("active");
    });
    $('.visao .btn').on( "mouseout", function(){
        $(this).find(".tooltip").removeClass("active");
    });

    $('.visao .modal .btn-fechar').on('click', function (){
        $('.visao .modal').removeClass("active");
    });
    $('.visao .modal').on('click', function (){
        $('.visao .modal').removeClass("active");
    });

    // // modal
    $('.visao .btn').on("click", function(){
        $('.visao .modal').removeClass("active");
        // $(this).addClass("active");
        // troca o slide
        var modal = $(this).data('modal');
        // $('.slide').removeClass("active");
        $(modal).addClass("active");
    });

    function modalMobile() {
        if($(window).width() <= 500){
            $('.visao .modal .btn-fechar').on('click', function (){
                $('.visao .modal').removeClass("active");
                
                $('html').css({'overflow':'hidden auto'});
            });
            $('.visao .modal').on('click', function (){
                $('.visao .modal').removeClass("active");
                $('html').css({'overflow':'hidden auto'});
            });
        
            // // modal
            $('.visao .btn').on("click", function(){
                $('.visao .modal').removeClass("active");
                // troca o slide
                var modal = $(this).data('modal');
                // $('.slide').removeClass("active");
                $(modal).addClass("active");

                $('html').css({'overflow':'hidden'});
            });
        }
    }
    modalMobile();
});
$( window ).resize(function() {
    modalMobile();
});