
var i = 0;
$('.brand-cases-slider').each(function() {
  $(this).slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    // appendArrows: '#brand-slider-btn-block' +  i,
    prevArrow: '<button type="button" class="slick-prev slick-arrow-btn"></button>',
    nextArrow: '<button type="button" class="slick-next slick-arrow-btn"></button>',
    responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
            lidesToScroll: 1,
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        }
      ]
  });
  i++;
});

// FUNCIONAMENTO DE BOTOES DE FILTRO DE MARCAS
let choiceBtn = $('.brand-cases-btn');

// Loop through the buttons and add the active class to the current/clicked button
for (var i = 0; i < choiceBtn.length; i++) {
choiceBtn[i].addEventListener("click", function() {
  var current = document.getElementsByClassName("item-active");
  current[0].className = current[0].className.replace(" item-active", "");
  this.className += " item-active";
});
}

// EXIBICAO SLIDER DE MARCAS
// jQuery(function(){
//  jQuery('.brand-cases-btn').click(function(){
//         jQuery('.brand-cases-slider-content').removeClass('visible');
//         jQuery('#brand-slider'+$(this).attr('target')).addClass('visible');
//         jQuery('.brand-cases-slider').slick('setPosition');  
//  });
// });

// EXIBICAO DE LISTA DE MARCAS
jQuery(function(){
  jQuery('.brand-cases-btn').click(function(){
    jQuery('.brand-list').removeClass('visible');
    jQuery('#brand-list'+$(this).attr('target')).addClass('visible');  
  });
  jQuery('#brand-btn0').click(function(){
    jQuery('#brand-list0').addClass('visible');
  })
});