$(document).ready(function(){
    // grafico 1
    $('.btn-icon').on( "mouseover", function(){
        $(this).find(".tooltip").addClass("active");
    });
    $('.btn-icon').on( "mouseout", function(){
        $(this).find(".tooltip").removeClass("active");
    });

    // gargalos
    $('.gargalo-link').on("click", function(){
        $('.gargalo-link').removeClass("active");
        $(this).addClass("active");
        // troca o slide
        var slide = $(this).data('slide');
        $('.slide').removeClass("active");
        $(slide).addClass("active");
    });
    function gargaloMobile() { 
        if($(window).width() <= 800){
            // gargalos
            if($('.gargalo-link:first-child').hasClass('active')) {
                let conteudo = $('.slide:first-child .content p').text();
                // console.log(conteudo);
                $('.gargalo-link:first-child').append("<div class='slide-value'>" + "<p>" + conteudo + "</p>" + '<div class="btn-slide">saiba como resolver</div>' + "</div>")
            }
            $('.gargalo-link').on("click", function(){
               
                // troca o slide
                var slide = $(this).data('slide');
                var slideValue = $(slide).find( ".content p" ).text();
                console.log(slideValue);
                $('.slide-value').remove()
                $(this).append("<div class='slide-value'>" + "<p>" + slideValue + "</p>" + '<div class="btn-slide">saiba como resolver</div>' + "</div>")
                
                $(this).find('.btn-slide').click(function (){
                    $('html, body').animate({
                        scrollTop: $("#visao").offset().top
                    }, 2000);
                });
            });
        }
    }

    gargaloMobile();
    


    // SLIDER DE CASES
$('.sliderCasesContent').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    arrows: false,
    fade: false,
    asNavFor: '.sliderCasesLogo'
});
$('.sliderCasesLogo').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.sliderCasesContent',
    dots: false,
    nextArrow: '<button type="button" class="slick-next"><img src="https://lozinskyconsultoria.com.br/wp-content/themes/webexpresso/images/right-arrow-btn.png"></button>',
    prevArrow: '<button type="button" class="slick-prev"><img src="https://lozinskyconsultoria.com.br/wp-content/themes/webexpresso/images/left-arrow-btn.png"></button>',
    centerMode: true,
    focusOnSelect: true,
    responsive: [
        {
        breakpoint: 500,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
        }
        },
    ]
});
// FIM SLIDER DE CASES

});
// FUNCIONALIDADES PARA MOBILE
$( window ).resize(function() {
    gargaloMobile();
});
