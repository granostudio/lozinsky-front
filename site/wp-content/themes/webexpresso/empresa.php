<?php
/**
* Template Name: Empresa
*
* @package WebExpresso
* @subpackage Grano Studio
* @since Grano Studio 1.0
*/
get_header();
?>

<div class="breadcumbs">
        <div class="wrapperContent">
        <?php
                        if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb( '<p id="breadcumbsText">','</p>' );
                        }
                        ?>
        </div>
    </div>
    <section class="enterprise" id="enterprise">
        <a href="#mission" class="linkToDown">
            <div class="link-bar"></div>
            <p class="linkText">scroll</p>
        </a>
        <div class="enterprise-bg" style="background-image: url( <?php echo get_template_directory_uri() ?>/images/empresa/quem-somos-banner.jpg)">
            <div class="wrapperContent">
                <!-- <p class="cat-text">Sobre nós</p>
                <h3 class="post-title">A matéria-prima dos nossos serviços é o futuro</h3>
                <a href="#modalVideo" rel="modal:open" class="linkVideo">
                    <svg version="1.0" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    width="109.279px" height="36.542px" viewBox="0 0 109.279 36.542" enable-background="new 0 0 109.279 36.542"
                    xml:space="preserve">
               <g id="guides">
               </g>
               <g id="lg-site">
                   <g>
                       <g>
                           <defs>
                               <rect id="SVGID_46_" x="-181.887" y="245.267" width="1200" height="541.402"/>
                           </defs>
                           <clipPath id="SVGID_2_">
                               <use xlink:href="#SVGID_46_"  overflow="visible"/>
                           </clipPath>
                           <g id="JvdQb4.tif_2_" clip-path="url(#SVGID_2_)">
                               
                                   <image overflow="visible" width="1200" height="786" id="Camada_1_9_" xlink:href="C339896EE48E83E4.png"  transform="matrix(1.0008 0 0 1.0008 -182.3496 11.6689)">
                               </image>
                           </g>
                       </g>
                       <g>
                           <path fill="none" class="play-button-svg" stroke="#FFFFFF" stroke-miterlimit="10" d="M104.376,6.59h-67.48c-2.99,0-5.857-1.028-8.287-2.771
                               C25.546,1.623,21.757,0.375,17.67,0.51C8.611,0.81,1.107,8.073,0.536,17.119c-0.652,10.337,7.539,18.924,17.735,18.924
                               c3.542,0,6.841-1.038,9.611-2.826c2.137-1.378,4.661-2.033,7.203-2.033h69.29c2.422,0,4.403-1.981,4.403-4.403V10.993
                               C108.779,8.571,106.798,6.59,104.376,6.59z"/>
                           <path fill="#FFFFFF" class="play-icon-svg" d="M12.851,27.947V9.827c0-1.395,1.604-2.179,2.705-1.322l11.642,9.06c0.862,0.671,0.862,1.975,0,2.646
                               l-11.642,9.06C14.455,30.125,12.851,29.341,12.851,27.947z"/>
                           
                               <text transform="matrix(1 0 0 1 38.0889 23.3154)" fill="#FFFFFF" font-family="'Montserrat-Light'" font-size="15.2944" letter-spacing="1">assista</text>
                       </g>
                   </g>
               </g>
               </svg>
               
                </a> -->
                <a href="#modalVideo" rel="modal:open" class="linkVideo">
                    <div class="play-button">
                        
                    </div>
                </a>
            </div>
        </div>
        <?php if( get_field('video_top') ): ?>
            <div id="modalVideo" class="modal">
                <?php the_field('video_top') ?>
                <a href="#" rel="modal:close"></a>
            </div>
        <?php endif;?>
    </section>
    <section class="about-block">
        <div class="wrapperContent">
            <div class="about-content">
                <h3 class="videoTitle">Sobre a Lozinsky</h3>
                <p>
                    A Lozinsky Consultoria atua na resolução de
                    problemas complexos, posicionando a TI como
                    pilar estratégico de negócios e eliminando
                    as âncoras que limitam o crescimento das
                    organizações. Possui uma equipe multidisciplinar,
                    experiente e que reúne habilidades complexas,
                    como amplo conhecimento sobre os processos de
                    negócio em empresas de diversos setores.
                </p>
            </div>
        </div>
    </section>
    <section class="mission" id="mission">
        <div class="wrapperContent">
            <div class="mission-options-block">
        
                <div class="option-text-item item-active" target="1">
                    <div class="option-text">Missão</div>
                </div>
                
                <div class="option-text-item" target="2">
                    <div class="option-text">Visão</div>
                </div>
                <div class="option-text-item" target="3">
                    <div class="option-text">Valores</div>
                </div>
                
            </div>
            <div class="mission-info-block" id="mission-info-block">
                <div class="mission-info-item visible" id="infoItem1">
                    <ul class="mission-info-list info-list-single">
                        <li class="info-list-item">Contribuir para a transformação das organizações na busca por mais produtividade e competitividade.</li>
                    </ul>
                </div>
                <div class="mission-info-item" id="infoItem2">
                    <ul class="mission-info-list info-list-single">
                        <li class="info-list-item">Sermos reconhecidos como uma empresa de serviços de consultoria que se destaca pelos talentos e experiências nas áreas de gestão e de TI, e que é capaz de desenvolver com sucesso soluções personalizadas para cada cliente.</li>
                    </ul>
                </div>
                <div class="mission-info-item" id="infoItem3">
                    <ul class="mission-info-list">
                        <li class="info-list-item">Independência na elaboração das nossas recomendações técnicas.</li>
                        <li class="info-list-item">Comprometimento com as metas definidas em cada projeto.</li>
                        <li class="info-list-item">Capacidade de comprovar a viabilidade da implantação do que recomendamos.</li>
                    </ul>
                    <ul class="mission-info-list">
                        <li class="info-list-item">Construção de parcerias que complementam os nossos serviços com empresas que compartilham dos nossos  valores.</li>
                        <li class="info-list-item">Estudos e pesquisas constantes para atualização dos nossos conhecimentos sobre gestão e TI.</li>
                        <li class="info-list-item">Manutenção do relacionamento com os clientes, mesmo em períodos nos quais não estamos desenvolvendo projetos.</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="associate">
        <div class="wrapperContent">
            <div class="associate-top-block">
                <h3 class="text-roboto text-xl text-bold">Conheça os nossos sócios-consultores</h3>
                <p class="text-sm text-light text-mont">A Lozinsky possui uma equipe multidisciplinar, experiente e que reúne habilidades complexas, como amplo conhecimento sobre os processos de negócio em empresas de diversos setores.</p>
            </div>
            <div class="associate-list">

            <?php
            // BUSCA APENAS POR USARIOS QUE POSSUEM O CHECKBOX DE CONSULTOS ATIVO (ADVANCED FIELDS)
            $blogusers = get_users( array(
                'meta_query' => array(
                    array(
                        'key' => 'status_consultant',
                        'compare' => '=',
                        'value' => '1',
                        'orderby'  => 'login',
                        'order' => 'ASC'
                    )
                )
            ));
            // Array of WP_User objects.

            
            foreach ( $blogusers as $user ): ?>

                <div class="associate-item">
                    <!-- VERIFICA SE USUARIO TEM IMAGEM -->
                    <?php $user_photo = get_field("foto",  'user_'.$user->ID);?>
                            <?php if(($user_photo == "" )): ?>
                                <?php $photo = "#" ?>
                            <?php else: ?>
                                <?php $photo = $user_photo ?>
                            <?php endif; ?>
                    <a href="<?php echo get_author_posts_url( $user->ID ); ?>">
                        <div class="associate-img" style="background-image: url(<?php echo $photo; ?>);background-size: contain;">
    
                        </div>
                    </a>
                    <div class="associate-info">
                        <div class="top-info-block">
                                <!-- VERIFICA SE USUARIO POSSUI NOME COMPLETO -->
                                <?php if(($user->first_name == "" ) || ($user->last_name == "") ): ?>
                                    <?php $nome = $user->user_nicename; ?>
                                <?php else: ?>
                                    <?php $nome = $user->first_name . " " . $user->last_name ?>
                                <?php endif; ?>
                            <h3 class="text-lg text-bold text-mont"><?php echo $nome; ?></h3>
                                <!-- VERIFICA SE USUARIO TEM OCUPACAO -->
                                <?php $user_occupation = get_field("occupation",  'user_'.$user->ID);?>
                                <?php if(($user_occupation == "" )): ?>
                                    <?php $occupation = "" ?>
                                <?php else: ?>
                                    <?php $occupation = $user_occupation ?>
                                <?php endif; ?>
                            <p class="text-sm text-light text-mont"><?php echo $occupation ?></p>
                            <!-- <p class="text-sm text-light text-mont">Sócio-fundador e CEO</p> -->
                        </div>
                        <div class="middle-info-block">
                            <!-- VERIFICA SE USUARIO TEM DESCRICAO -->
                                <?php if(($user->description == "" )): ?>
                                    <?php $descricao = "" ?>
                                <?php else: ?>
                                    <?php $descricao = $user->description ?>
                                <?php endif; ?>
                            <p class="text-sm text-light text-mont"><?php echo $descricao ?></p>
                        </div>
                        <div class="footer-info-block">
                            <!-- <a href="#">
                                <p class="see-more">leia mais</p>
                            </a> -->
                            <!-- VERIFICA SE USUARIO TEM LINKEDIN -->
                            <?php $user_linkedin = get_field("linkedin",  'user_'.$user->ID);?>
                            <?php if(($user_linkedin == "" )): ?>
                                <?php $linkedin = "#" ?>
                            <?php else: ?>
                                <?php $linkedin = $user_linkedin ?>
                            <?php endif; ?>
                            <a href="<?php echo $linkedin; ?>">
                                <div class="footer-social-item">
                                    <i class="fab fa-linkedin-in"></i>
                                </div>
                            </a>
                            <a href="<?php echo get_author_posts_url( $user->ID ); ?>" class="footer-link-text">
                                <p class="footer-link-content">Leia os artigos</p>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- <div class="associate-item">
                    <div class="associate-img" style="background-image: url(images/sergio_01.png);background-size: contain;">

                    </div>
                    <div class="associate-info">
                        <div class="top-info-block">
                            <h3 class="text-lg text-bold text-mont">Sérgio Lozinsky</h3>
                            <p class="text-sm text-light text-mont">Sócio-fundador e CEO</p>
                        </div>
                        <div class="middle-info-block">
                            <p class="text-sm text-light text-mont">Com mais de 30 anos na TI, é fundador da Lozinsky Consultoria. Autor de livros e inúmeros artigos sobre estratégia empresarial e tecnologia. </p>
                        </div>
                        <div class="footer-info-block">
                            <a href="#">
                                <p class="see-more">leia mais</p>
                            </a>
                            <a href="#">
                                <div class="footer-social-item">
                                    <i class="fab fa-linkedin-in"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div> -->
                <?php endforeach; ?>
            </div>
        </div>
    </section>

    <section class="team">
        <div class="teamBg"></div>
        <div class="wrapperContent">
            <div class="team-info">
                <h4 class="text-xl text-bold text-roboto">
                    Faça parte do nosso time!
                </h4>
                <p class="text-md text-mont">
                    A matéria-prima dos nossos serviços é o futuro.
                    E para pensar a longo prazo sem perder de vista os objetivos urgentes, precisamos de pessoas que gostem de resolver problemas. Afinal, os desafios das organizações, a fim de se manterem eficientes e competitivas, não são poucos - muito menos simples.
                </p>
                
                <p class="team-btn"><a href="<?php echo get_permalink(113) ?>" class="text-mont text-bold">saiba mais</a></p>
                
            </div>
        </div>
        <div class="team-cover" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/pessoas_conversando-02.jpg)">
            <div class="team-cover-info">
                <p class="text-mont text-bold title-md">“Livre pensar <br> é só pensar”</p>
                <p class="text-mont text-sm text-bold">Millôr Fernandes</p>
            </div>
        </div>
    </section>

<?php
get_footer();
?>