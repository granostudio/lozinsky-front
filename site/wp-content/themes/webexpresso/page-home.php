<?php
/**
* Template Name: Home
*
* @package WebExpresso
* @subpackage Grano Studio
* @since Grano Studio 1.0
*/
get_header();
?>

<!-- ################# SLIDER TOP BLOCK ################# -->
<div class="sliderTop" id="sliderTop">
        <a href="#cases" class="linkToDown">
            <div class="link-bar"></div>
            <p class="linkText">scroll</p>
        </a>
        <div class="sliderTopContent">
            <?php
                if( have_rows('banner_principal') ):

                    // loop through the rows of data
                   while ( have_rows('banner_principal') ) : the_row();
               
                    // vars
                    $imagem = get_sub_field('imagem');
                    $categoria = get_sub_field('categoria');
                    $titulo = get_sub_field('titulo');
                    $titulo_2 = get_sub_field('titulo_2');
                    $estilo_do_botao = get_sub_field('estilo_do_botao');
                    $link = get_sub_field('link');

                    ?>
                    <div class="sliderTopContentItem" style="background-image: url(<?php echo $imagem ?>)">
                        <div class="wrapperContent">
                            <div class="contentItemInfo lightInfo">
                                <p class="catItem"><?php echo $categoria ?></p>
                                <h2 class="titleItem"><?php echo $titulo ?></h2>
                                <p class="textItem"><?php echo $titulo_2 ?></p>
                                <?php
                                    if($estilo_do_botao == "Assista"){
                                       ?>
                                        <a href="<?php echo $link ?>" class="linkVideo">
                                            <?php include "svg-buttons/assista-btn.php"; ?>
                                        </a>
                                       <?php
                                    } 
                                    if($estilo_do_botao == "Ao Vivo") { ?>
                                        <a href="<?php echo $link;?>" class="linkReadMore">
                                            <?php include "svg-buttons/inscreva-se-ao-vivo.php"; ?>                    
                                        </a>
                                        <?php
                                    }else {
                                        ?>
                                        <a href="<?php echo $link;?>" class="linkReadMore">
                                            <?php include "svg-buttons/inscreva-se-btn.php"; ?>                      
                                        </a>
                                        <?php
                                    }
                                ?>
                                
                            </div>
                        </div>
                    </div>
                    <?php

                   endwhile;
                endif;

            ?>
        </div>
        <div class="sliderTopControl">
            <div class="arrowBlock"></div>
            <div class="sliderTopInfo"></div>
        </div>
    </div>
    <!-- ############# END SLIDER TOP BLOCK ############ -->
    <!-- ######## slider de cases ######### -->
    <section class="sliderCasesBlock" id="cases">
        <div class="wrapperContent">
            <h2 class="sliderCasesTitle">Casos de sucesso</h2>
        </div>
        <div class="wrapperContent">
            <?php $case_args = array( 'post_type' => 'cases', 'posts_per_page' => -1 );
            $case_query = new WP_Query( $case_args ); 
            ?>
            <div class="sliderCasesLogo">
            <?php if ( $case_query->have_posts() ) : ?>
                <?php while ( $case_query->have_posts() ) : $case_query->the_post(); ?>
                        <?php if( get_field('logo_case') ): ?>
                            <div class="sliderCasesLogoItem"><img src="<?php the_field('logo_case'); ?>" alt=""></div>
                            <!-- <div class="sliderCasesLogoItem"><img src="<?php echo get_template_directory_uri() ?>/images/cpfl-energia.png" alt=""></div> -->
                        <?php endif; ?>
                <?php endwhile; ?>
            <?php endif; ?>
            </div>
            <div class="sliderCasesContent">
            <?php if ( $case_query->have_posts() ) : ?>
                <?php while ( $case_query->have_posts() ) : $case_query->the_post(); ?>
                <div class="sliderCasesContentInfo">
                    <div class="contentImageBlock">
                        <?php if( get_field('cover_video') ): ?>
                            <div class="video-cover" style="background-image: url(<?php the_field('cover_video'); ?>)"></div>
                        <?php endif; ?>
                        <?php if( get_field('video_case_pilula') ): ?>

                            <!-- CONFIGURACOES ADICIONAIS DO VIDEO -->
                            <?php 

                                $iframe = get_field('video_case_pilula');

                                // use preg_match to find iframe src
                                preg_match('/src="(.+?)"/', $iframe, $matches);
                                $src = $matches[1];


                                // add extra params to iframe src
                                $params = array(
                                    'controls'    => 0,
                                    'hd'        => 1,
                                    'autohide'    => 1,
                                    'autoplay'   => 0
                                );

                                $new_src = add_query_arg($params, $src);

                                $iframe = str_replace($src, $new_src, $iframe);


                                // add extra attributes to iframe html
                                $attributes = 'frameborder="0"';

                                $iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);


                                // echo $iframe
                               
                            
                            ?>

                            <?php  echo $iframe; ?>
                        <?php else: ?>
                            <div class="caseBg" style="background-image: url(<? echo get_the_post_thumbnail_url() ?>)"></div>
                        <?php endif; ?>
                    </div>
                    <div class="contentInfoBlock">
                        <div class="contentInfoBlockContent">
                            <h4 class="contentInfoCaseTitle"><?php the_field('brand_name'); ?></h4>
                            <a href="<?php echo get_permalink(); ?>"><h3 class="contentInfoTitle"><?php the_title(); ?></h3></a>
                            <?php if( get_field('text_intro') ): ?>
                                <div class="contentInfoText"><?php the_field('text_intro'); ?></div>
                            <?php else: ?>
                                <div class="contentInfoText"><?php echo mb_strimwidth(get_the_content(),0,400, '...');  ?></div>
                            <? endif; ?>
                            <?php if( get_field('video_case') ): ?>
                                <a href="<?php echo get_permalink(); ?>" class="linkCase"><p class="linkCaseText">assista ao case completo</p></a>
                            <?php else: ?>
                                <a href="<?php echo get_permalink(); ?>" class="linkCase"><p class="linkCaseText">leia o case completo</p></a>
                            <? endif; ?>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            <?php endif; ?>
                
                <!-- <div class="sliderCasesContentInfo">
                    <div class="contentImageBlock" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/video-case.png)"></div>
                    <div class="contentInfoBlock">
                        <div class="contentInfoBlockContent">
                            <h4 class="contentInfoCaseTitle">unidas frotas</h4>
                            <h3 class="contentInfoTitle">arcu cursus vitae</h3>
                            <p class="contentInfoText">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Fermentum et sollicitudin ac orci phasellus egestas tellus rutrum tellus. Congue quisque egestas diam in arcu cursus. Mauris augue neque gravida in fermentum et sollicitudin. Sit amet volutpat consequat mauris nunc congue nisi vitae suscipit. Habitant morbi tristique senectus et netus et. Egestas maecenas pharetra convallis posuere morbi leo. Aliquam nulla facilisi cras fermentum odio eu feugiat pretium. Eget nunc scelerisque viverra mauris in. Nullam eget felis eget nunc lobortis. Volutpat est velit egestas dui id ornare. Sed pulvinar proin gravida hendrerit lectus a. Quis auctor elit sed vulputate mi.</p>
                            <a href="#" class="linkCase"><p class="linkCaseText">assista o case completo</p></a>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </section>
    <!-- ###### fim slider de cases ##### -->
    <section class="visionIt">
        <div class="wrapperContent">
            <div class="visionTitleBlock">
                <a href="<?php echo get_permalink(90); ?>">
                    <h3 class="visionTitle">Uma visão <br> sistêmica sobre a TI</h3>
                </a>
                <div class="visionTextBlock">
                    <p class="visionText">Como desenhar a arquitetura de sistemas e a infraestrutura mais adequadas para a sua empresa?</p>
                </div>
            </div>
           
        </div>
    </section>

    <!-- BLOCO NOVO DE VISAO -->
    <section class="visao" id="visao">
                    
        <div class="wrapperContent">
            <h3 class="title-lg barra-cinza">Como a <br>Lozinsky Consultoria<br>resolve esses problemas</h3>
            <ul class="graphic-itens-list">
                <li class="graphic-itens-list-item">
                    <h5 class="graphic-title">Estratégia de TI</h5>
                    <p class="graphic-text">Qual a melhor maneira de direcionar recursos, esforços e investimentos de TI para apoiar as estratégias do negócio? <a href="#estrategia_ti" rel="modal:open">- Leia mais</a></p>
                </li>
                <li class="graphic-itens-list-item">
                    <h5 class="graphic-title">Arquitetura de TI</h5>
                    <p class="graphic-text">Qual o modelo de sistemas, infraestrutura tecnológica e organização de TI mais adequado para a sua empresa?</p>
                    <p class="graphic-text">Como aumentar a eficiência dos processos de negócios? <a href="#arquitetura_ti" rel="modal:open">- Leia mais</a></p>
                </li>
                <li class="graphic-itens-list-item">
                    <h5 class="graphic-title">Governança</h5>
                    <p class="graphic-text">Como organizar os investimentos em TI conforme o planejamento estratégico e as prioridades definidas pelas lideranças do negócio? <a href="#governanca" rel="modal:open">- Leia mais</a></p>
                </li>
                <li class="graphic-itens-list-item">
                    <h5 class="graphic-title">Gestão de projetos</h5>
                    <p class="graphic-text">Como assegurar que os projetos aprovados pela empresa estejam estruturados corretamente e preparados para atingirem os objetivos planejados? <a href="#gestao_projetos" rel="modal:open">- Leia mais</a></p>
                </li>
                <li class="graphic-itens-list-item">
                    <h5 class="graphic-title">Operações</h5>
                    <p class="graphic-text">Como garantir o desempenho, a segurança, a continuidade e a contingência das operações de TI, mantendo a estabilidade dos processos de negócios? <a href="#operacoes" rel="modal:open">- Leia mais</a> </p>
                </li>
            </ul>
            

        </div>
    
        </div>
            <div class="grafico">
                <img src="<?php echo get_template_directory_uri() ?>/images/atuacao/globe.png" alt="">
                <div class="btn btn-1" data-modal=".modal-1">
                    <a href="#estrategia_ti" rel="modal:open">
                        <div class="ball"></div>
                    </a>
                    <div class="tooltip">
                        <!-- <h4>Estratégia de TI</h4> -->
                        <!-- <p>Qual a melhor maneira de direcionar recursos, esforços e investimentos de TI para apoiar as estratégias do negócio?</p> -->
                        <a href="#estrategia_ti" rel="modal:open">Clique no círculo</a>
                    </div>
                </div>
                <div class="btn btn-2" data-modal=".modal-2">
                    <a href="#arquitetura_ti" rel="modal:open">
                        <div class="ball"></div>
                    </a>
                    <div class="tooltip">
                        <!-- <h4>Arquitetura de TI</h4>
                        <p>Qual o modelo de sistemas, infraestrutura tecnológica e organização de TI mais adequado para a sua empresa?</p>
                        <p>Como aumentar a eficiência dos processos de negócios?</p> -->
                        <a href="#arquitetura_ti" rel="modal:open">Clique no círculo</a>
                    </div>
                </div>
                <div class="btn btn-3" data-modal=".modal-3">
                    <a href="#gestao_projetos" rel="modal:open">
                        <div class="ball"></div>
                    </a>
                    <div class="tooltip">
                        <!-- <h4>Gestão de projetos</h4>
                        <p>Como assegurar que os projetos aprovados pela empresa estejam estruturados corretamente e preparados para atingirem os objetivos planejados?</p> -->
                        <a href="#gestao_projetos" rel="modal:open">Clique no círculo</a>
                    </div>
                </div>
                <div class="btn btn-4" data-modal=".modal-4">
                    <a href="#governanca" rel="modal:open">
                        <div class="ball"></div>
                    </a>
                    <div class="tooltip">
                        <!-- <h4>Governança</h4>
                        <p>Como organizar os investimentos em TI conforme o planejamento estratégico e as prioridades definidas pelas lideranças do negócio?</p> -->
                        <a href="#governanca" rel="modal:open">Clique no círculo</a>
                    </div>
                </div>
                <div class="btn btn-5" data-modal=".modal-5">
                    <a href="#operacoes" rel="modal:open">
                        <div class="ball"></div>
                    </a>
                    <div class="tooltip">
                        <!-- <h4>Operações</h4>
                        <p>Como garantir o desempenho, a segurança, a continuidade e a contingência das operações de TI, mantendo a estabilidade dos processos de negócios ?</p> -->
                        <a href="#operacoes" rel="modal:open">Clique no círculo</a>
                    </div>
                </div>
            </div>

            <div id="estrategia_ti" class="modal modal-lozinsky">
                <div class="modal-block">
                    <div class="menu-modal-block">
                        <a href="#estrategia_ti" class="menu-modal-item menu-item-active" rel="modal:open">Estratégia de TI</a>
                        <a href="#arquitetura_ti" class="menu-modal-item" rel="modal:open">Arquitetura de TI</a>
                        <a href="#governanca" class="menu-modal-item" rel="modal:open">Governança</a>
                        <a href="#gestao_projetos" class="menu-modal-item" rel="modal:open">Gestão de projetos</a>
                        <a href="#operacoes" class="menu-modal-item" rel="modal:open">Operações</a>
                    </div>
                    <div class="modal-content">
                        <h2 class="modal-title">Estratégia de TI</h2>
                        <a href="#operacoes" class="arrow-modal arrow-left" rel="modal:open"><img src="<?php echo get_template_directory_uri() ?>/images/atuacao/arrow-left-purple.png" alt=""></a>
                        <a href="#arquitetura_ti" class="arrow-modal arrow-right" rel="modal:open"><img src="<?php echo get_template_directory_uri() ?>/images/atuacao/arrow-right-purple.png" alt=""></a>
                        <div class="modal-content-block">
                            <div class="modal-text-block">
                                <p class="font-bold">Qual a melhor maneira de direcionar recursos, esforços e investimentos de TI para apoiar as estratégias do negócio?</p>
                                <p>Nossos projetos avaliam e recomendam ações em áreas como:</p>
                                <ul>
                                    <li>Organização de TI</li>
                                    <li>Processos de negócios</li>
                                    <li>Arquitetura de sistemas e serviços</li>
                                    <li>Governança</li>
                                    <li>Infraestrutura e comunicação</li>
                                    <li>Projetos de TI</li>
                                </ul>
                                <ul>
                                    <p>Entregas documentadas:</p>
                                    <li>Diagnóstico</li>
                                    <li>Plano de Ação detalhado para os próximos anos</li>
                                    <li>Sumário Executivo e Business Case</li>
                                </ul>
                            </div>
                            <div class="modal-image-block">
                                <img src="<?php echo get_template_directory_uri() ?>/images/atuacao/modal-image-1.png" alt="">
                            </div>
                        </div>
                        <div class="modal-contact-block">
                            <a href="<?php echo get_permalink(107); ?>">
                                <p>Entre em contato</p>
                            </a>
                        </div>
                    <a href="#" class="close-modal-btn" rel="modal:close">fechar</a>
                    </div>
                </div>
            </div>
            <div id="arquitetura_ti" class="modal modal-lozinsky"> 
                <div class="modal-block">
                    <div class="menu-modal-block">
                        <a href="#estrategia_ti"   class="menu-modal-item" rel="modal:open">Estratégia de TI</a>
                        <a href="#arquitetura_ti"  class="menu-modal-item menu-item-active" rel="modal:open">Arquitetura de TI</a>
                        <a href="#governanca" class="menu-modal-item" rel="modal:open">Governança</a>
                        <a href="#gestao_projetos"      class="menu-modal-item" rel="modal:open">Gestão de projetos</a>
                        <a href="#operacoes"       class="menu-modal-item" rel="modal:open">Operações</a>
                    </div>
                    <div class="modal-content">
                        <h2 class="modal-title">Arquitetura de TI</h2>
                        <a href="#estrategia_ti" class="arrow-modal arrow-left" rel="modal:open"><img src="<?php echo get_template_directory_uri() ?>/images/atuacao/arrow-left-purple.png" alt=""></a>
                        <a href="#governanca" class="arrow-modal arrow-right" rel="modal:open"><img src="<?php echo get_template_directory_uri() ?>/images/atuacao/arrow-right-purple.png" alt=""></a>
                        <div class="modal-content-block">
                            <div class="modal-text-block">
                                <p class="font-bold">Qual o modelo de sistemas, infraestrutura tecnológica e organização de TI mais adequado para a sua empresa?</p>
                                <p class="font-bold">Como aumentar a eficiência dos processos de negócios?</p>
                                <p>A Lozinsky atua de forma a:</p>
                                <ul>
                                    <li>Entender e desenhar a cadeia de valor da empresa</li>
                                    <li>Analisar os processos críticos e seu papel na cadeia de valor</li>
                                    <li>Entender a arquitetura de sistemas (atual e futura) e suas integrações</li>
                                    <li>Entender a arquitetura de infraestrutura e de comunicações (atual e futura)</li>
                                    <li>Selecionar soluções de tecnologia para ampliar a automação dos processos de negócios</li>
                                    <li>Desenhar um modelo de serviços integrados, considerando organização de TI, parceiros e fornecedores de tecnologia</li>
                                    <li>Planejar os recursos de TI para atender às demandas estratégicas do negócio</li>
                                    <li>Avaliar e propor impacto orçamentário das propostas, considerando CAPEX e OPEX</li>
                                    <li>Definir plano de ação e business case para implantação da arquitetura proposta</li>
                                </ul>
                            </div>
                            <div class="modal-image-block">
                                <img src="<?php echo get_template_directory_uri() ?>/images/atuacao/modal-image-2.png" alt="">
                            </div>
                        </div>
                        <div class="modal-contact-block">
                            <a href="<?php echo get_permalink(107); ?>">
                                <p>Entre em contato</p>
                            </a>
                        </div>
                    <a href="#" class="close-modal-btn" rel="modal:close">fechar</a>
                    </div>
                </div>
            </div>
            <div id="governanca" class="modal modal-lozinsky">
                <div class="modal-block">
                    <div class="menu-modal-block">
                        <a href="#estrategia_ti"   class="menu-modal-item" rel="modal:open">Estratégia de TI</a>
                        <a href="#arquitetura_ti"  class="menu-modal-item" rel="modal:open">Arquitetura de TI</a>
                        <a href="#governanca"      class="menu-modal-item menu-item-active" rel="modal:open">Governança</a>
                        <a href="#gestao_projetos" class="menu-modal-item" rel="modal:open">Gestão de projetos</a>
                        <a href="#operacoes"       class="menu-modal-item" rel="modal:open">Operações</a>
                    </div>
                    <div class="modal-content">
                        <h2 class="modal-title">Governança</h2>
                        <a href="#arquitetura_ti" class="arrow-modal arrow-left" rel="modal:open"><img src="<?php echo get_template_directory_uri() ?>/images/atuacao/arrow-left-purple.png" alt=""></a>
                        <a href="#gestao_projetos" class="arrow-modal arrow-right" rel="modal:open"><img src="<?php echo get_template_directory_uri() ?>/images/atuacao/arrow-right-purple.png" alt=""></a>
                        <div class="modal-content-block">
                            <div class="modal-text-block">
                                <p class="font-bold">Como organizar os investimentos em TI conforme o planejamento estratégico e as prioridades definidas pelas lideranças do negócio?</p>
                                <p>Apoiada nas melhores práticas de mercado, a Lozinsky propõe e implementa um conjunto de processos adaptados à cultura e ao tamanho da empresa.</p>
                                <p>Gestão da demanda:</p>
                                <ul>
                                    <li>Portfólio de Projetos</li>
                                    <li>Gestão de Projetos</li>
                                    <li>Gestão de Mudança</li>
                                    <li>Comitê de TI</li>
                                </ul>
                                <p>Organização de TI:</p>
                                <ul>
                                    <li>Definição de Catálogos de Serviços</li>
                                    <li>Definição de SLA's</li>
                                    <li>Organização de Service Desk e Suporte a Aplicações</li>
                                    <li>Definição de Perfil da Equipe de TI</li>
                                    <li>Dimensionamento da Equipe de TI</li>
                                </ul>
                            </div>
                            <div class="modal-image-block">
                                <img src="<?php echo get_template_directory_uri() ?>/images/atuacao/modal-image-3.png" alt="">
                            </div>
                        </div>
                        <div class="modal-contact-block">
                            <a href="<?php echo get_permalink(107); ?>">
                                <p>Entre em contato</p>
                            </a>
                        </div>
                    <a href="#" class="close-modal-btn" rel="modal:close">fechar</a>
                    </div>
                </div>
            </div>
            <div id="gestao_projetos" class="modal modal-lozinsky">
                <div class="modal-block">
                    <div class="menu-modal-block">
                        <a href="#estrategia_ti"   class="menu-modal-item" rel="modal:open">Estratégia de TI</a>
                        <a href="#arquitetura_ti"  class="menu-modal-item" rel="modal:open">Arquitetura de TI</a>
                        <a href="#governanca"      class="menu-modal-item" rel="modal:open">Governança</a>
                        <a href="#gestao_projetos" class="menu-modal-item menu-item-active" rel="modal:open">Gestão de projetos</a>
                        <a href="#operacoes"       class="menu-modal-item" rel="modal:open">Operações</a>
                    </div>
                    <div class="modal-content">
                        <h2 class="modal-title">Gestão de projetos</h2>
                        <a href="#governanca" class="arrow-modal arrow-left" rel="modal:open"><img src="<?php echo get_template_directory_uri() ?>/images/atuacao/arrow-left-purple.png" alt=""></a>
                        <a href="#operacoes" class="arrow-modal arrow-right" rel="modal:open"><img src="<?php echo get_template_directory_uri() ?>/images/atuacao/arrow-right-purple.png" alt=""></a>
                        <div class="modal-content-block">
                            <div class="modal-text-block">
                                <p class="font-bold">Como assegurar que os projetos aprovados pela empresa estejam estruturados corretamente e preparados para atingirem os objetivos planejados?</p>
                                <p>Projetos são a base da transformação dos negócios. Por isso, a Lozinsky Consultoria atua diretamente na melhoria do modelo de gestão de projetos da empresa, revisando e alinhando objetivos, escopo e equipe, e também atuando para minimizar os riscos identificados, sejam eles de ordem técnica ou funcional. Podemos atuar em diversas etapas desse ciclo:</p>
                                <ul>
                                    <li>Priorização de projetos e gestão do portfólio</li>
                                    <li>Estruturação de escritório de projetos (PMO)</li>
                                    <li>Adaptação de metodologias de gestão e de desenvolvimento de projetos à realidade de cada empresa</li>
                                    <li>Seleção e implantação de ferramentas de apoio à gestão de projetos</li>
                                    <li>Apoio na seleção de equipes para gestão de projetos</li>
                                    <li>Suporte na gestão de projetos complexos</li>
                                    <li>Avaliação de riscos e da qualidade em projetos</li>
                                    <li>Recuperação de projetos problemáticos</li>
                                </ul>
                            </div>
                            <div class="modal-image-block">
                                <img src="<?php echo get_template_directory_uri() ?>/images/atuacao/modal-image-4.png" alt="">
                            </div>
                        </div>
                        <div class="modal-contact-block">
                            <a href="<?php echo get_permalink(107); ?>">
                                <p>Entre em contato</p>
                            </a>
                        </div>
                    <a href="#" class="close-modal-btn" rel="modal:close">fechar</a>
                    </div>
                </div>
            </div>
            <div id="operacoes" class="modal modal-lozinsky">
            <div class="modal-block">
                    <div class="menu-modal-block">
                        <a href="#estrategia_ti"   class="menu-modal-item" rel="modal:open">Estratégia de TI</a>
                        <a href="#arquitetura_ti"  class="menu-modal-item" rel="modal:open">Arquitetura de TI</a>
                        <a href="#governanca"      class="menu-modal-item" rel="modal:open">Governança</a>
                        <a href="#gestao_projetos" class="menu-modal-item" rel="modal:open">Gestão de projetos</a>
                        <a href="#operacoes"       class="menu-modal-item menu-item-active" rel="modal:open">Operações</a>
                    </div>
                    <div class="modal-content">
                        <h2 class="modal-title">Operações</h2>
                        <a href="#gestao_projetos" class="arrow-modal arrow-left" rel="modal:open"><img src="<?php echo get_template_directory_uri() ?>/images/atuacao/arrow-left-purple.png" alt=""></a>
                        <a href="#estrategia_ti" class="arrow-modal arrow-right" rel="modal:open"><img src="<?php echo get_template_directory_uri() ?>/images/atuacao/arrow-right-purple.png" alt=""></a>
                        <div class="modal-content-block">
                            <div class="modal-text-block">
                                <p class="font-bold">Como garantir o desempenho, a segurança, a continuidade e a contingência das operações de TI, mantendo a estabilidade dos processos de negócios?</p>
                                <p>A minimização de incidentes e a estabilidade dos sistemas em operação são fundamentais para que a TI possa dedicar-se a melhorar e construir as soluções demandadas pelo negócio. E não adianta falar em governança, melhores práticas e cumprimento do orçamento se o usuário vivencia problemas nos seus processos. Dessa forma, a Lozinsky Consultoria atua no diagnóstico e na prevenção de problemas na infraestrutura tecnológica da empresa, como:</p>
                                <ul>
                                    <li>Revisar contratos de serviços de missão crítica no data center</li>
                                    <li>Revisar ou definir o Catálogo de Serviços de TI e dos Acordos de Níveis de Serviço (SLAs)</li>
                                    <li>Definir o modelo de sourcing</li>
                                    <li>Revisar e otimizar contratos de serviços de TI</li>
                                    <li>Avaliar o processo de gestão de incidentes</li>
                                    <li>Avaliar ou planejar o Service Desk</li>
                                    <li>Avaliar os serviços (internos e externos) de suporte ao ambiente de operações da TI</li>
                                    <li>Elaborar e coordenar RFPs para contratação de diversos serviços (Cloud Services, Service Desk, AMS, Projetos e Fábrica de Software)</li>
                                </ul>
                            </div>
                            <div class="modal-text-block">
                                <ul>
                                    <li>Revisar licenciamentos de software e a obsolescência dos equipamentos</li>
                                    <li>Revisar a topologia da rede e avaliar o dimensionamento dos equipamentos e dos serviços</li>
                                    <li>Elaborar requerimentos de conhecimentos e experiências necessários para suportar o ambiente e recomendar formação ou contratação dos perfis requeridos</li>
                                    <li>Desenhar o processo de gestão de mudanças no ambiente operacional (GMUD)</li>
                                </ul>
                                <p>Segurança da informação e mitigação de riscos:</p>
                                <ul>
                                    <li>Avaliação dos riscos e identificação de ações emergenciais para colocar o patamar de riscos operacionais em nível aceitável</li>
                                    <li>Elaboração das políticas de segurança</li>
                                    <li>Perfil de acesso à rede e às aplicações</li>
                                    <li>Controle de licenças e contratos</li>
                                    <li>Aderência às regras da Lei Geral de Proteção de Dados (LGPD)</li>
                                    <li>Otimização do desempenho das operações de TI</li>
                                    <li>Avaliação das soluções de continuidade (como redundâncias, balanceamento de cargas, etc)</li>
                                    <li>Revisão ou elaboração do Disaster Recovery Planning (DRP)</li>
                                </ul>
                            </div>
                        </div>
                        <div class="modal-contact-block">
                            <a href="<?php echo get_permalink(107); ?>">
                                <p>Entre em contato</p>
                            </a>
                        </div>
                    <a href="#" class="close-modal-btn" rel="modal:close">fechar</a>
                    </div>
                </div>
            </div>
    </section>
    <!-- FIM BLOCO VISAO -->

    <section class="articleBlock">
        <div class="wrapperContent">
            <h3 class="articleTitle">últimos artigos</h3>
        </div>
        <div class="wrapperContent">
            <div class="articlePostBlock">
                <!-- QUERY DE POST -->
                <?php $post_args = array( 'post_type' => 'post', 'posts_per_page' => 3 );
                $post_query = new WP_Query( $post_args ); 
                ?>
                <?php if ( $post_query->have_posts() ) : ?>
                    <?php while ( $post_query->have_posts() ) : $post_query->the_post(); ?>
                        <div class="articlePostItem">
                        <!-- IMAGEM DESTACADA -->
                        <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>
                            <div class="articleItemImg" style="background-image: url(<?php echo $featured_img_url ?>)">
                        
                            <?php
                                //get all the categories the post belongs to
                                $categories = wp_get_post_categories( get_the_ID() );
                                //loop through them
                                foreach($categories as $c){
                                $cat = get_category( $c );
                                //get the name of the category
                                $cat_id = get_cat_ID( $cat->name );
                                //make a list item containing a link to the category
                                echo '<a href="'.get_category_link($cat_id).'"><p class="articleCatText">'.$cat->name.'</p></a>';
                                }
                            ?>
                            <?php
                            $author_id = get_the_author_meta( 'ID' );
                            $author_photo = get_field("foto",  'user_'.$author_id);?>
                            <a href="<?php echo get_author_posts_url( $author_id ); ?>">
                                <div class="articleItemAuthor" style="background-image: url(<?php echo $author_photo ?>)"></div>
                            </a>
                        </div>
                        <div class="articleItemBlock">
                            <a href="<?php the_permalink(); ?>">
                                <h3 class="articleItemTitle"> <?php echo get_the_title() ?></h3>
                            </a>
                            <?php $content = get_the_excerpt();?>
                            <!-- LIMITA O CONTENT EM 180 CARACTERES -->
                            <!-- <div class="articleItemText"><?php //echo mb_strimwidth($content, 0, 180, '...'); ?></div> -->
                            <a href="<?php the_permalink(); ?>" style="color: #353233;">
                                <div class="articleItemText"><?php echo wp_strip_all_tags($content);?></div>
                            </a>
                            <!-- <a href="<?php //the_permalink(); ?>"><p class="articleItemReadMore">Leia o artigo</p></a> -->
                            <a href="<?php the_permalink(); ?>"><p class="articleItemReadMore"><?php the_field('texto_do_link'); ?></p></a>
                        </div>
                        <div class="articleItemFooter">
                            <p class="articleItemFooterText articleItemDate"><?php echo get_the_date(); ?></p>
                            <!-- <p class="articleItemFooterText articleItemCountComents">Nenhum comentário</p> -->
                        </div>
                     </div>
                     <?php endwhile; 
                        wp_reset_postdata();
                        else : ?>
                        <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
                    <?php endif; ?>
                
            </div>
            <div class="articleBlockButton">
                <!-- LINK PARA A PAGINA DE BLOG -->
                <a href="<?php echo get_permalink(61) ?>" class="articleBtnLink">
                    <div class="articleBtn">mais artigos</div>
                </a>
            </div>
        </div>

<?php
get_footer();
?>