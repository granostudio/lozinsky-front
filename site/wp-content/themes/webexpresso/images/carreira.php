<?php
/**
* Template Name: Carreira
*
* @package WebExpresso
* @subpackage Grano Studio
* @since Grano Studio 1.0
*/
get_header();
?>
<div class="breadcumbs">
        <div class="wrapperContent">
        <?php
                        if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb( '<p id="breadcumbsText">','</p>' );
                        }
                        ?>
        </div>
    </div>
    <section class="career">
        <div class="part-of-the-team">
            <div class="wrapperContent">
                <div class="pott-info">
                    <p class="cat-text cat-line-right">Faça parte do nosso time!</p>
                    <h2 class="text-xl">A matéria-prima dos nossos serviços é o futuro.</h2>
                    <p class="career-text">E para pensar a longo prazo sem perder de vista os objetivos urgentes, precisamos de pessoas que gostem de resolver problemas. Afinal, os desafios das organizações, a fim de se manterem eficientes e competitivas, não são poucos - muito menos simples</p>
                    <p class="career-text">Nossa missão é contribuir para a transformação das empresas, posicionando a TI como pilar de crescimento e longevidade dos negócios. Se você se identifica com a proposta, saiba:</p>
                </div>
                <div class="pott-img-block" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/carreira/career-img.jpg)">
                    <div class="pott-img-content">
                        <h2 class="pott-img-title">“Livre pensar é só pensar”</h2>
                        <p class="text-sm">Millôr Fernandes</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="what-we-expect">
                <div class="pott-img-block" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/carreira/career-img.jpg)">
            <div class="what-we-expect-img-block" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/carreira/career-img2.jpg)">
                <h3 class="wwe-img-title">O que esperamos de você</h3>
            </div>
            <div class="what-we-expect-info">
                <div class="wwe-info-circle-block">
                    <div class="info-circle-item eye">
                        <div class="info-circle-text-block">
                            <p class="info-circle-text">Visão de que trabalho e carreira</p>
                            <p class="info-circle-text">são fontes de crescimento, realização e felicidade</p>
                        </div>
                    </div>
                    <div class="info-circle-item person">
                        <div class="info-circle-text-block">
                            <p class="info-circle-text">Energia e entusiasmo</p>
                        </div>
                    </div>
                    <div class="info-circle-item arm">
                        <div class="info-circle-text-block">
                            <p class="info-circle-text">Força e execução</p>
                        </div>
                    </div>
                    <div class="info-circle-item hand">
                        <div class="info-circle-text-block">
                            <p class="info-circle-text">Atitude positivamente questionadora</p>
                        </div>
                    </div>
                    <div class="info-circle-item shoe">
                        <div class="info-circle-text-block">
                            <p class="info-circle-text">Visão sistêmica</p>
                            <p class="info-circle-text">e pensamento ágil</p>
                        </div>
                    </div>
                    <div class="info-circle-item lamp">
                        <div class="info-circle-text-block">
                            <p class="info-circle-text">Ímpeto inovador</p>
                            <p class="info-circle-text">e transformador</p>
                            <p class="info-circle-text">(agente de mudança)</p>
                        </div>
                    </div>
                    <div class="info-circle-item group">
                        <div class="info-circle-text-block">
                            <p class="info-circle-text">Experiência profissional</p>
                            <p class="info-circle-text">é desejável</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="what-to-expect">
            <h3 class="text-xl">O que você pode esperar de nós</h3>
            <div class="what-to-expect-list">
                <div class="wrapperContent">
                    <div class="what-to-expect-list-content">
                        <div class="what-to-expect-list-item">
                            <p class="text-sm">Ambiente de trabalho enriquecedor, com portunidades de crescimento e realização</p>
                        </div>
                        <div class="what-to-expect-list-item">
                            <p class="text-sm">Visão de um propósito por trás de todo projeto desenvolvido</p>
                        </div>
                        <div class="what-to-expect-list-item">
                            <p class="text-sm">Modelo de trabalho que valoriza autonomia e liberdade com responsabilidade e compromisso</p>
                        </div>
                        <div class="what-to-expect-list-item">
                            <p class="text-sm">Experiência de trabalhar com empresas grandes e relevantes no mercado</p>
                        </div>
                        <div class="what-to-expect-list-item">
                            <p class="text-sm">Vivência e aprendizado com profissionais que compartilham dos mesmos valores</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="opportunities">
        <div class="wrapperContent">
            <div class="op-info">
                <div class="op-info-top">
                    <h3 class="text-xl">Oportunidades</h3>
                    <p class="text-md">Estamos sempre em busca de profissionais que tenham o nosso perfil. Entre em contato conosco pelo formulário abaixo e envie o seu currículo.</p>
                </div>
                <div class="op-info-btn-block">
                    <div class="op-info-btn-item info-btn-active" target="1">
                        <h4 class="op-info-btn-title">VAGA</h4>
                        <p class="op-info-btn-text">Descrição da vaga</p>
                    </div>
                    <div class="op-info-btn-item" target="2">
                        <h4 class="op-info-btn-title">VAGA</h4>
                        <p class="op-info-btn-text">Descrição da vaga</p>
                    </div>
                    <div class="op-info-btn-item" target="3">
                        <h4 class="op-info-btn-title">VAGA</h4>
                        <p class="op-info-btn-text">Descrição da vaga</p>
                    </div>
                    <div class="op-info-btn-item" target="4">
                        <h4 class="op-info-btn-title">VAGA</h4>
                        <p class="op-info-btn-text">Descrição da vaga</p>
                    </div>
                    <div class="op-info-btn-item" target="5">
                        <h4 class="op-info-btn-title">VAGA</h4>
                        <p class="op-info-btn-text">Descrição da vaga</p>
                    </div>
                    <div class="op-info-btn-item" target="6">
                        <h4 class="op-info-btn-title">VAGA</h4>
                        <p class="op-info-btn-text">Descrição da vaga</p>
                    </div>
                </div>
            </div>
            <div class="op-form-block">
                <form class="op-form" action="#">
                    <input type="text" class="op-input" placeholder="nome">
                    <input type="email" class="op-input" placeholder="e-mail">
                    <input type="tel" class="op-input" placeholder="telefone">
                    <input type="file" name="file" id="file" class="inputfile" placeholder="currículo">
                    <label for="file" class="op-input">currículo</label>
                    <button type="submit" class="btn-send">enviar</button>
                </form>
            </div>
        </div>
        <div class="wrapperContent">
            <div class="op-info-description">
                <div class="op-info-description-item desc-active" id="infoItem1">
                    <h4 class="op-info-desc-title">VAGA SELECIONADA</h4>
                    <p class="op-info-desc-text">Donec a ante id justo dictum aliquet condim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vehicula dapibus lacus, in venenatis lectus ultricies eget. Donec a ante id justo dictum aliquet Donec a ante id justo dictum aliquet condim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vehicula dapibus lacus, in venenatis lectus ultricies eget. Donec a ante id justo dictum aliquet condim Donec a ante id justo dictum aliquet condim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vehicula dapibus lacus, in venenatis lectus ultricies eget. Donec a ante id justo dictum aliquet condim Donec a ante id justo dictum aliquet condim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vehicula dapibus lacus, in venenatis lectus ultricies eget. Donec a ante id justo dictum aliquet condim Donec a ante id justo dictum aliquet condim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vehicula dapibus lacus, in venenatis lectus ultricies eget. Donec a ante id justo dictum aliquet.</p>
                </div>
                <div class="op-info-description-item" id="infoItem2">
                    <h4 class="op-info-desc-title">VAGA SELECIONADA 2 </h4>
                    <p class="op-info-desc-text">Donec a ante id justo dictum aliquet condim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vehicula dapibus lacus, in venenatis lectus ultricies eget. Donec a ante id justo dictum aliquet Donec a ante id justo dictum aliquet condim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vehicula dapibus lacus, in venenatis lectus ultricies eget. Donec a ante id justo dictum aliquet condim Donec a ante id justo dictum aliquet condim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vehicula dapibus lacus, in venenatis lectus ultricies eget. Donec a ante id justo dictum aliquet condim Donec a ante id justo dictum aliquet condim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vehicula dapibus lacus, in venenatis lectus ultricies eget. Donec a ante id justo dictum aliquet condim Donec a ante id justo dictum aliquet condim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vehicula dapibus lacus, in venenatis lectus ultricies eget. Donec a ante id justo dictum aliquet.</p>
                </div>
                <div class="op-info-description-item" id="infoItem3">
                    <h4 class="op-info-desc-title">VAGA SELECIONADA 3</h4>
                    <p class="op-info-desc-text">Donec a ante id justo dictum aliquet condim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vehicula dapibus lacus, in venenatis lectus ultricies eget. Donec a ante id justo dictum aliquet Donec a ante id justo dictum aliquet condim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vehicula dapibus lacus, in venenatis lectus ultricies eget. Donec a ante id justo dictum aliquet condim Donec a ante id justo dictum aliquet condim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vehicula dapibus lacus, in venenatis lectus ultricies eget. Donec a ante id justo dictum aliquet condim Donec a ante id justo dictum aliquet condim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vehicula dapibus lacus, in venenatis lectus ultricies eget. Donec a ante id justo dictum aliquet condim Donec a ante id justo dictum aliquet condim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vehicula dapibus lacus, in venenatis lectus ultricies eget. Donec a ante id justo dictum aliquet.</p>
                </div>
            </div>
        </div>
    </section>
<?php
get_footer();
?>