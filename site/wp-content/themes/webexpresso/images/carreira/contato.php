<?php
/**
* Template Name: Contato
*
* @package WebExpresso
* @subpackage Grano Studio
* @since Grano Studio 1.0
*/
get_header();
?>
<div class="breadcumbs">
        <div class="wrapperContent">
        <?php
                        if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb( '<p id="breadcumbsText">','</p>' );
                        }
                        ?>
        </div>
    </div>
    <section class="contact-main">
        <div class="purple-bg"></div>
        <div class="wrapperContent">
            <div class="form-block">
                <h2 class="text-mont title-lg">Entre em contato conosco</h2>
                <form action="">
                    <input class="contact-input" type="text" name="nome" id="" placeholder="Nome">
                    <input class="contact-input" type="tel" name="tel" id="" placeholder="Telefone">
                    <input class="contact-input" type="email" name="email" id="" placeholder="Email">
                    <textarea class="contact-textarea" name="mensagem" id="" cols="30" rows="10" placeholder="Mensagem"></textarea>
                    <!-- Rounded switch -->
                    <div class="switch-block">
                        <label class="switch">
                            <input type="checkbox">
                            <span class="slider round"></span>
                        </label>
                        <p class="switch-text">Receber novidades por email</p>
                    </div>
                    <button class="contact-submit" type="submit">Enviar</button>
                </form>
            </div>
            <div class="conection-block">
                <h3 class="title-md text-mont">outras maneiras de conexão</h3>
                <p class="text-lg text-thin">Inscreva-se em nossa newsletter</p>
                <p class="text-sm text-thin">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad</p>
                <p class="text-lg text-thin">Acompanhe-nos nas redes sociais</p>
                <p class="text-sm text-thin">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad </p>
                <div class="social-block">
                    <ul class="social-list">
                        <li class="social-item"><a href="<?php echo get_field('linkedin_site', 'option'); ?>"><i class="fab fa-linkedin-in"></i></a></li>
                        <li class="social-item"><a href="<?php echo get_field('facebook_site', 'option'); ?>"><i class="fab fa-facebook-f"></i></a></li>
                        <li class="social-item"><a href="<?php echo get_field('twitter_site', 'option'); ?>"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>

            </div>
        </div>
    </section>

    <section class="associate">
        <div class="wrapperContent">
            <div class="associate-top-block">
                <h3 class="text-roboto text-xl text-bold">conheça os nossos sócios-consultores</h3>
                <p class="text-sm text-light text-mont">A Lozinsky atua em diversos segmentos de mercado com uma equipe formada por profissionais experientes e com amplo conhecimento dos processos das diversas áreas de negócios de uma empresa</p>
            </div>
            <div class="associate-list">
            <?php
            // BUSCA APENAS POR USARIOS QUE POSSUEM O CHECKBOX DE CONSULTOS ATIVO (ADVANCED FIELDS)
            $blogusers = get_users( array(
                'meta_query' => array(
                    array(
                        'key' => 'status_consultant',
                        'compare' => '=',
                        'value' => '1'
                    )
                )
            ));
            // Array of WP_User objects.
            
            foreach ( $blogusers as $user ): ?>
                <div class="associate-item">
                    <!-- VERIFICA SE USUARIO TEM IMAGEM -->
                    <?php $user_photo = get_field("foto",  'user_'.$user->ID);?>
                            <?php if(($user_photo == "" )): ?>
                                <?php $photo = "#" ?>
                            <?php else: ?>
                                <?php $photo = $user_photo ?>
                            <?php endif; ?>
                    <div class="associate-img" style="background-image: url(<?php echo $photo; ?>);background-size: contain;">

                    </div>
                    <div class="associate-info">
                        <div class="top-info-block">
                                <!-- VERIFICA SE USUARIO POSSUI NOME COMPLETO -->
                                <?php if(($user->first_name == "" ) || ($user->last_name == "") ): ?>
                                    <?php $nome = $user->user_nicename; ?>
                                <?php else: ?>
                                    <?php $nome = $user->first_name . " " . $user->last_name ?>
                                <?php endif; ?>
                            <h3 class="text-lg text-bold text-mont"><?php echo $nome; ?></h3>
                                <!-- VERIFICA SE USUARIO TEM OCUPACAO -->
                                <?php $user_occupation = get_field("occupation",  'user_'.$user->ID);?>
                                <?php if(($user_occupation == "" )): ?>
                                    <?php $occupation = "" ?>
                                <?php else: ?>
                                    <?php $occupation = $user_occupation ?>
                                <?php endif; ?>
                            <p class="text-sm text-light text-mont"><?php echo $occupation ?></p>
                            <!-- <p class="text-sm text-light text-mont">Sócio-fundador e CEO</p> -->
                        </div>
                        <div class="middle-info-block">
                            <!-- VERIFICA SE USUARIO TEM DESCRICAO -->
                                <?php if(($user->description == "" )): ?>
                                    <?php $descricao = "" ?>
                                <?php else: ?>
                                    <?php $descricao = $user->description ?>
                                <?php endif; ?>
                            <p class="text-sm text-light text-mont"><?php echo $descricao ?></p>
                        </div>
                        <div class="footer-info-block">
                            <!-- <a href="#">
                                <p class="see-more">leia mais</p>
                            </a> -->
                            <!-- VERIFICA SE USUARIO TEM LINKEDIN -->
                            <?php $user_linkedin = get_field("linkedin",  'user_'.$user->ID);?>
                            <?php if(($user_linkedin == "" )): ?>
                                <?php $linkedin = "#" ?>
                            <?php else: ?>
                                <?php $linkedin = $user_linkedin ?>
                            <?php endif; ?>
                            <a href="<?php echo $linkedin; ?>">
                                <div class="footer-social-item">
                                    <i class="fab fa-linkedin-in"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- <div class="associate-item">
                    <div class="associate-img" style="background-image: url(images/sergio_01.png);background-size: contain;">

                    </div>
                    <div class="associate-info">
                        <div class="top-info-block">
                            <h3 class="text-lg text-bold text-mont">Sérgio Lozinsky</h3>
                            <p class="text-sm text-light text-mont">Sócio-fundador e CEO</p>
                        </div>
                        <div class="middle-info-block">
                            <p class="text-sm text-light text-mont">Com mais de 30 anos na TI, é fundador da Lozinsky Consultoria. Autor de livros e inúmeros artigos sobre estratégia empresarial e tecnologia. </p>
                        </div>
                        <div class="footer-info-block">
                            <a href="#">
                                <p class="see-more">leia mais</p>
                            </a>
                            <a href="#">
                                <div class="footer-social-item">
                                    <i class="fab fa-linkedin-in"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div> -->
                <?php endforeach; ?>
            </div>
        </div>
    </section>

    <section class="team">
        <div class="teamBg"></div>
        <div class="wrapperContent">
            <div class="team-info">
                <h4 class="text-xl text-bold text-roboto">
                    Faça parte do nosso time!
                </h4>
                <p class="text-md text-mont">
                    A matéria-prima dos nossos serviços é o futuro.
                    E para pensar a longo prazo sem perder de vista os objetivos urgentes, precisamos de pessoas que gostem de resolver problemas. Afinal, os desafios das organizações, a fim de se manterem eficientes e competitivas, não são poucos - muito menos simples.
                </p>
                
                <p class="team-btn"><a href="<?php echo get_permalink(113) ?>" class="text-mont text-bold">saiba mais</a></p>
                
            </div>
        </div>
        <div class="team-cover" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/pessoas_conversando-02.jpg)">
            <div class="team-cover-info">
                <p class="text-mont text-bold title-md">“Livre pensar <br> é só pensar”</p>
                <p class="text-mont text-sm text-bold">Millôr Fernandes</p>
            </div>
        </div>
    </section>
<?php
get_footer();
?>