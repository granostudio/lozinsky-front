<?php
/**
* The template for displaying the header
*
* Displays all of the head element and everything up until the "site-content" div.

*
* @package WebExpresso
* @subpackage Grano Studio
* @since Grano Studio 1.0
*/


?>

<!-- HEADER -->
<!DOCTYPE html>

<html lang="UTF-8" >
<head>
<base href="/">
   <meta charset="<?php bloginfo( 'charset' ); ?>">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <link rel="profile" href="http://gmpg.org/xfn/11">
   <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
   <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
   <?php endif; ?>
   <?php wp_head(); ?>


<?php
$favicon = get_field('fav_icon', 'option');;
?>
   <link rel="shortcut icon" type="image/png" href="<?php echo $favicon; ?>">

<!-- Get Scripts -->
<?php
$scripts = get_field('adicionar_script', 'option');
if($scripts)
{
    foreach($scripts as $script)
   {
       if($script['posicao']=="Header"){
           echo $script['script'];
       }
    }
}
?>
</head><!-- /Header -->
<body id="bodyLozinsky">


<div class="menuLozinsky">
        <div class="wrapperContent">
            <div class="logo-menu">
                <a href="<?php echo site_url(); ?>">
                    <img src="<?php echo get_field('logo', 'option') ?>" alt="<?php echo get_bloginfo(); ?>" style="width:100%">
                </a>
            </div>
            <div class="menu-button">
                <div class="menu-button-bar"></div>
                <div class="menu-button-bar"></div>
            </div>
            <nav class="menu-nav">
                <div class="menu-list-block">
                        <?php if ( has_nav_menu( 'primary' ) ) : ?>
							<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?>">
								<?php
									wp_nav_menu(
										array(
											'theme_location' => 'primary',
											'menu_class' => 'menu-list',
										)
									);
								?>
							</nav><!-- .main-navigation -->
						<?php endif; ?>
                    <!-- <ul class="menu-list">
                        <li class="menu-list-item"><a href="#">home</a></li>
                        <li class="menu-list-item"><a href="#">empresa</a></li>
                        <li class="menu-list-item"><a href="#">cases</a></li>
                        <li class="menu-list-item"><a href="#">reports</a></li>
                        <li class="menu-list-item"><a href="#">atuação</a></li>
                        <li class="menu-list-item"><a href="#">blog</a></li>
                        <li class="menu-list-item"><a href="#">carreira</a></li>
                    </ul> -->
                    <ul class="social-list">
                        <a href="<?php echo get_field('linkedin_site', 'option'); ?>" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                        <a href="<?php echo get_field('facebook_site', 'option'); ?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
                        <a href="<?php echo get_field('twitter_site', 'option'); ?>" target="_blank"><i class="fab fa-twitter"></i></a>
                        <a href="<?php echo get_field('youtube_site', 'option'); ?>" target="_blank"><i class="fab fa-youtube"></i></a>
                    </ul>
                </div>
            </nav>
        </div>
    </div>


