<?php
/**
 * Category Page
 * 
 * @package WebExpresso
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */

get_header(); ?>



<div class="breadcumbs">
    <div class="wrapperContent">
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<p id="breadcumbsText">','</p>' );
        }
        ?>
    </div>
</div>
<!-- CONTEUDO DO POST -->
<?php if ( have_posts() ) : ?>
<section class="main-post" id="main-post">
        <div class="wrapperContent">
            <div class="cat-title-block">
                <?php
                    the_archive_title( '<h2 class="cat-title cat-line">', '</h2>' );
                    
                ?>
            </div>
        </div>
            
</section>
<section class="articleBlock">
    <div class="wrapperContent">
        <div class="articlePostBlock">
            <?php while ( have_posts() ) : the_post(); ?>
                <div class="articlePostItem">
                <!-- IMAGEM DESTACADA -->
                <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>
                    <div class="articleItemImg" style="background-image: url(<?php echo $featured_img_url ?>)">
                
                    <?php
                        //get all the categories the post belongs to
                        $categories = wp_get_post_categories( get_the_ID() );
                        //loop through them
                        foreach($categories as $c){
                        $cat = get_category( $c );
                        //get the name of the category
                        $cat_id = get_cat_ID( $cat->name );
                        //make a list item containing a link to the category
                        echo '<a href="'.get_category_link($cat_id).'"><p class="articleCatText">'.$cat->name.'</p></a>';
                        }
                    ?>
                    <?php
                    $author_id = get_the_author_meta( 'ID' );
                    $author_photo = get_field("foto",  'user_'.$author_id);?>
                    <p><?php $author_photo ?></p>
                        <!-- <div class="articleItemAuthor" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/author-img.jpg)"></div> -->
                        <div class="articleItemAuthor" style="background-image: url(<?php echo $author_photo ?>)"></div>
                    </div>
                    <div class="articleItemBlock">
                        <a href="<?php the_permalink(); ?>"><h3 class="articleItemTitle"> <?php echo get_the_title() ?></h3></a>
                        <?php $content = get_the_excerpt();?>
                        <!-- LIMITA O CONTENT EM 180 CARACTERES -->
                        <div class="articleItemText"><?php echo wp_strip_all_tags($content);?></div>
                        <a href="<?php the_permalink(); ?>"><p class="articleItemReadMore">Leia o artigo</p></a>
                    </div>
                    <div class="articleItemFooter">
                        <p class="articleItemFooterText articleItemDate"><?php echo get_the_date(); ?></p>
                        <!-- <p class="articleItemFooterText articleItemCountComents">Nenhum comentário</p> -->
                    </div>
                    </div>
                    <?php endwhile; 
                    wp_reset_postdata();
                    else : ?>
                    <p class="not-found"><img src="<?php echo get_template_directory_uri() ?>/images/not-found.png" alt=""></p>
                <?php endif; ?>
            
        </div>
        <div class="articleBlockButton">
            <div href="#" class="articleBtnLink">
                <div class="articleBtn">
                    <?php
                    echo paginate_links( array(
                        'format'          => 'page/%#%',
                        'current'         => $paged,
                        // 'total'           => $max_num_pages,
                        'mid_size'        => 2,
                        'prev_text'       => __('<'),
                        'next_text'       => __('>')
                    ) );
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
get_footer();
