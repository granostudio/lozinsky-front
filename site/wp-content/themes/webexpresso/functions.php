<?php
/**
 * Grano Studio functions and definitions
 *
 *
 * @package WebExpresso
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */

// if ( ! function_exists( 'webexpresso_setup' ) ) :


	// SEGURANÇA
	// mudar id o admin
	$user_admin = get_user_by('ID','1');
	if(!empty($user_admin)){
		global $wpdb;
		$wpdb->update( $table_prefix.'users', array( 'ID' => "1024"), array( 'ID' => "1") );
		$wpdb->update( $table_prefix.'usermeta', array( 'user_id' => "1024"), array( 'user_id' => "1") );
	}

	remove_action('wp_head','wp_generator');

	//atualização automatica dos plugins
	add_filter( 'auto_update_plugin', '__return_true' );

	// remover/esconder versão so wp
	remove_action('wp_head', 'wp_generator');



/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 */
function webexpresso_setup() {

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );


	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1200, 9999 );


	// MENU CONFIGURAÇ˜AO =======================================================

    // This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Menu Superior', 'webexpresso' ),
		'footer'  => __( 'Rodapé', 'webexpresso' ),
	) );

	// Check if the menu exists
	$menu_name = 'Menu Superior';
	$menu_exists = wp_get_nav_menu_object( $menu_name );

	// If it doesn't exist, let's create it.
	if( !$menu_exists){
	    $menu_id = wp_create_nav_menu($menu_name);

		// Set up default menu items
	    wp_update_nav_menu_item($menu_id, 0, array(
	        'menu-item-title' =>  __('Home'),
	        'menu-item-classes' => 'home',
	        'menu-item-url' => home_url( '/' ),
	        'menu-item-status' => 'publish'));

	}

	// /MENU CONFIGURAÇ˜AO =====================================================

	// SIBABAR ===================================================================
	if ( function_exists('register_sidebar') )
		register_sidebar(array(
				'name'          => __( 'Sidebar Blog', 'webexpresso' ),
				'id'            => 'sidebar_blog',
				'description'   => '',
                'class'         => '',
				'before_widget' => '<li id="%1$s" class="widget %2$s">',
				'after_widget'  => '</li>',
				'before_title'  => '<h2 class="widgettitle">',
				'after_title'   => '</h2>'
	));
	// /SIBABAR ==================================================================

		/**
	 * Filter the except length to 20 characters.
	 *
	 * @param int $length Excerpt length.
	 * @return int (Maybe) modified excerpt length.
	 */
		function wpdocs_custom_excerpt_length( $length ) {
		    return 20;
		}
		add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );
		function wpdocs_excerpt_more( $more ) {
			return '...';
			// return '...<a href="'.get_permalink( get_the_ID() ).'">Leia mais.</a>';
		}
		add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );
	

	/*
	 *
	 * 
	 * Criar página de opções
	 */

	if( function_exists('acf_add_options_page') ) {
	
		acf_add_options_page(array(
			'page_title' 	=> 'Design',
			'menu_title'	=> 'Design',
			'menu_slug' 	=> 'design',
			'parent_slug'   => 'options-general.php',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));
		
		acf_add_options_page(array(
			'page_title' 	=> 'Header/Footer',
			'menu_title'	=> 'Header/Footer',
			'menu_slug' 	=> 'header-footer',
			'parent_slug'   => 'options-general.php',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));
		
		acf_add_options_page(array(
			'page_title' 	=> 'Destaques do Blog',
			'menu_title'	=> 'Destaques Blog',
			'menu_slug' 	=> 'banner-log',
			'position' 		=> 5,
			'icon_url'		=> 'dashicons-images-alt2',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));
		acf_add_options_page(array(
			'page_title' 	=> 'Destaques dos Cases',
			'menu_title'	=> 'Destaques Cases',
			'menu_slug' 	=> 'banner-cases',
			'position' 		=> 5,
			'icon_url'		=> 'dashicons-images-alt2',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));
	}
	

	//Admin customize
	include 'inc/grano-admincustomize.php';

	// desativar editor bloco para paginas



}
// endif; // webexpresso_setup
add_action( 'after_setup_theme', 'webexpresso_setup' );



/**
 * Enqueues scripts and styles.
 *
 */

function webexpresso_scripts() {

	// // Theme stylesheet.
	wp_enqueue_style( 'webexpresso-style', get_stylesheet_uri() );
	wp_enqueue_style( 'webexpresso-main', get_template_directory_uri(). '/css/main.css', array(), '1.0.0'  );

	wp_deregister_script( 'jquery' );
	wp_enqueue_script( 'webexpresso-jquery1-11',  'https://code.jquery.com/jquery-1.11.0.min.js',array(), '1.0.0', true);
	wp_enqueue_script( 'webexpresso-jquery1-migrate',  'https://code.jquery.com/jquery-migrate-1.2.1.min.js',array(), '1.0.0', true );
	wp_enqueue_script( 'webexpresso-carouseljs',  get_template_directory_uri(). '/js/plugins/slick.min.js',array(), '2.1.0', true );
	wp_enqueue_script( 'webexpresso-mainjs',  get_template_directory_uri(). '/js/main.js',array(), '1.3.0', true );
	wp_enqueue_script( 'webexpresso-homejs',  get_template_directory_uri(). '/js/home.js',array(), '1.3.0', true );
	wp_enqueue_script( 'webexpresso-scrolljs',  get_template_directory_uri(). '/js/scroll.js',array(), '1.3.0', true );
	
	if(is_home()||is_category()){
		// se pagina do home do blog
		wp_enqueue_style( 'webexpresso-blog', get_template_directory_uri(). '/css/blog.css' );
		wp_enqueue_script( 'modal-search-js', get_template_directory_uri(). '/js/modal.js',array(), '1.0', true );
	}
	if( is_page('Home') ) {
		wp_enqueue_style( 'webexpresso-style-visao', get_template_directory_uri(). '/css/visao.css' );
		wp_enqueue_script( 'webexpresso-visao-js',  get_template_directory_uri(). '/js/visao.js',array('webexpresso-jquery1-11'), '1.4', true );		

		wp_enqueue_style( 'jquery-modal-css', "https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" );
		wp_enqueue_script( 'jquery-modal-js', "https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js" ,array('webexpresso-jquery1-11'), '1.4', true );
	}
	if(is_single()||is_category()){
		// se pagina do home do blog
		wp_enqueue_style( 'webexpresso-single', get_template_directory_uri(). '/css/blog-interna.css' );
	}
	if(is_singular( 'post' )){
		wp_enqueue_script( 'single-post-js',  get_template_directory_uri(). '/js/single-post.js',array('webexpresso-jquery1-11'), '1.0', true );
	}
	if(is_page('quem-somos') || is_page('quem-somos-visual-alternativo')) {
		// se pagina empresa
		wp_enqueue_style( 'webexpresso-empresa', get_template_directory_uri(). '/css/empresa.css' );
		wp_enqueue_script( 'webexpresso-empresa-js',  get_template_directory_uri(). '/js/empresa.js',array('webexpresso-jquery1-11'), '1.0', true );
		// VIDEO MODAL
		wp_enqueue_style( 'video-modal-css', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css' );

		wp_enqueue_script( 'video-modal-js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js',array('webexpresso-jquery1-11'), '1.0', true );

	}
	if(is_page('Contato')) {
		// se pagina contato
		wp_enqueue_style( 'webexpresso-contato', get_template_directory_uri(). '/css/contato.css' );
		wp_enqueue_script( 'contato-js', get_template_directory_uri(). '/js/contato.js',array('webexpresso-jquery1-11'), '1.0', true );
	}
	if(is_page('Carreira')) {
		// se pagina Carreira
		wp_enqueue_style( 'webexpresso-carreira', get_template_directory_uri(). '/css/carreira.css' );
	}
	if(is_page('Cases')||is_singular( 'cases' ) ||is_archive( 'cases' )) {
		// se pagina Cases
		wp_enqueue_style( 'webexpresso-cases', get_template_directory_uri(). '/css/cases.css' );
		wp_enqueue_script( 'webexpresso-cases-js',  get_template_directory_uri(). '/js/cases.js',array('webexpresso-jquery1-11'), '1.1', true );

	}
	if(is_page('atuacao')) {
		// se pagina Cases
		wp_enqueue_style( 'webexpresso-atuacao', get_template_directory_uri(). '/css/atuacao.css' );
		wp_enqueue_script( 'webexpresso-visao-js',  get_template_directory_uri(). '/js/visao.js',array('webexpresso-jquery1-11'), '1.4', true );
		wp_enqueue_script( 'webexpresso-atuacao-js',  get_template_directory_uri(). '/js/atuacao.js',array('webexpresso-jquery1-11', 'webexpresso-carouseljs'), '1.11', true );
		// MODAL
		wp_enqueue_style( 'jquery-modal-css', "https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" );
		wp_enqueue_script( 'jquery-modal-js', "https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js" ,array('webexpresso-jquery1-11'), '1.4', true );
	}
	if(is_page('estudos')) {
		// se pagina Cases
		wp_enqueue_style( 'webexpresso-estudos', get_template_directory_uri(). '/css/estudos.css' );
	}
	// caroussel
	wp_enqueue_style( 'webexpresso-carrousel', get_template_directory_uri(). '/css/plugins/slick.css' );
	// wp_enqueue_style( 'webexpresso-carrousel-theme', get_template_directory_uri(). '/css/plugins/slick-theme.css' );
	// fontawsome
	wp_enqueue_style( 'webexpresso-fontawsome', "https://use.fontawesome.com/releases/v5.7.2/css/all.css" );
	
	// //Desabilitar jquery

}
add_action( 'wp_enqueue_scripts', 'webexpresso_scripts' );

// GERAR CAMPO DE BUSCA
/**
 * Generate custom search form
 *
 * @param string $form Form HTML.
 * @return string Modified form HTML.
 */
function wpdocs_my_search_form( $form ) {
    $form = '<form role="search" method="get" id="searchform" class="searchform" action="' . home_url( '/' ) . '" >
	<div class="search-content"><label class="screen-reader-text" for="s">' . __( 'Pesquisar por:' ) . '</label>
	<div class="searchform-content">
    <input type="text" class="search-field" value="' . get_search_query() . '" name="s" id="s" placeholder="Digite aqui..." />
	<input type="submit" id="searchsubmit" class="searchsubmit" value="'. esc_attr__( 'Pesquisar' ) .'" />
	</div>
    </div>
    </form>';
 
    return $form;
}
add_filter( 'get_search_form', 'wpdocs_my_search_form' );

// PAGINACAO RESULTADO DE PESQUISA
function search_filter($query) {
	if ( !is_admin() && $query->is_main_query() ) {
	  if ($query->is_search) {
		$query->set('paged', ( get_query_var('paged') ) ? get_query_var('paged') : 1 );
		$query->set('posts_per_page',6);
	  }
	}
  }