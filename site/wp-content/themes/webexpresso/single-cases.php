<?php
/**
 * 
 * Single Cases
 * 
 * @package WebExpresso
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */

get_header(); ?>

<div class="breadcumbs">
    <div class="wrapperContent">
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb( '<p id="breadcumbsText">','</p>' );
        }
        ?>
    </div>
</div>
<!-- CONTEUDO DO POST -->
<?php
			/* Start the Loop */
			while ( have_posts() ) :
                the_post();
                
                #vars
                $categories = get_the_taxonomies();
                $title = get_the_title();
                $content = get_the_content();
                $autor = get_the_author();
                $autor_photo = get_field("foto",  'user_'.get_the_author_meta('ID'));
                $autor_occupation = get_field("occupation",  'user_'.get_the_author_meta('ID'));
                $autor_description = get_the_author_meta('description');
                $autor_linkedin = get_field("linkedin",  'user_'.get_the_author_meta('ID'));
                #thumbanail
                $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
                // link tp share
                $actual_link = "http%3A//$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                $actual_link_postID = get_the_ID();

?>
<section class="case-interna">
    <div class="wrapperContent">
        <div class="case-top">
            <div class="case-top-info">
                <?php if(get_field('video_case')): ?>
                    <p class="text-lg text-bold text-mont category-text">case em vídeo</p>
                <?php else: ?>
                    <p class="text-lg text-bold text-mont category-text">case</p>
                <?php endif; ?>
                <p class="text-xl text-bold text-mont category-text-company"><?php echo get_field('brand_name'); ?></p>

                <p class="text-xl text-bold text-mont case-title"><?php echo esc_html($title); ?></p>
                <?php if( get_field('text_intro')): ?>
                    <div class="text-md text-mont case-text"><?php the_field('text_intro'); ?></div>
                <?php endif; ?>
            </div>
            <div class="case-top-midia">
                <?php if(get_field('video_case')): ?>
                    <?php echo get_field('video_case'); ?>
                <?php else: ?>
                    <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
                <?php endif; ?>
            </div>
        </div>
        <div class="case-content">
            <?php if(!empty($content)): ?>
                <div class="text-md text-mont"> <?php echo the_content(); ?></div>
            <?php ?>
            <?php else: ?>
            <div class="text-md text-mont"></div>
            <?php endif; ?>
        </div>
    </div>
</section>
            <?php endwhile; ?>
    <div class="control-post">
        <div class="wrapperContent">
            <div class="control-post-content">
                <?php previous_post_link( '%link', 'case anterior' ); ?>
                <?php next_post_link( '%link', 'próximo case' ); ?>
            </div>
        </div>
    </div>

    <section class="articleBlock">
            <div class="wrapperContent">
                <h3 class="articleTitle">outros cases</h3>
            </div>
            <div class="wrapperContent">
            <div class="articlePostBlock">
                <!-- QUERY DE POST -->
                <?php $post_args = array( 'post_type' => 'cases', 'posts_per_page' => 3, 'post__not_in' => array($actual_link_postID) );
                $post_query = new WP_Query( $post_args ); 
                ?>
                <?php if ( $post_query->have_posts() ) : ?>
                    <?php while ( $post_query->have_posts() ) : $post_query->the_post(); ?>
                        <div class="articlePostItem">
                        <!-- IMAGEM DESTACADA -->
                        <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>
                            <div class="articleItemImg" style="background-image: url(<?php echo $featured_img_url ?>)">
                        
                            <?php
                                //get all the categories the post belongs to
                                $categories = wp_get_post_categories( get_the_ID() );
                                //loop through them
                                foreach($categories as $c){
                                $cat = get_category( $c );
                                //get the name of the category
                                $cat_id = get_cat_ID( $cat->name );
                                //make a list item containing a link to the category
                                echo '<a href="'.get_category_link($cat_id).'"><p class="articleCatText">'.$cat->name.'</p></a>';
                                }
                            ?>
                            <?php
                            $author_id = get_the_author_meta( 'ID' );
                            $author_photo = get_field("foto",  'user_'.$author_id);?>
                            <!-- <div class="articleItemAuthor" style="background-image: url(<?php // echo $author_photo ?>)"></div> -->
                        </div>
                        <div class="articleItemBlock">
                            <a href="<?php the_permalink(); ?>"><h3 class="articleItemTitle"> <?php echo get_the_title() ?></h3></a>
                            <?php $content = get_the_content();?>
                            <!-- LIMITA O CONTENT EM 180 CARACTERES -->
                            <div class="articleItemText"><?php echo mb_strimwidth( wp_strip_all_tags($content), 0, 180, '...'); ?></div>
                            <a href="<?php the_permalink(); ?>"><p class="articleItemReadMore">leia mais</p></a>
                        </div>
                        <div class="articleItemFooter">
                            <p class="articleItemFooterText articleItemDate"><?php echo get_the_date(); ?></p>
                            <!-- <p class="articleItemFooterText articleItemCountComents">Nenhum comentário</p> -->
                        </div>
                     </div>
                     <?php endwhile; 
                        wp_reset_postdata();
                        else : ?>
                        <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
                    <?php endif; ?>
                
                </div>
                <div class="articleBlockButton">
                    <!-- LINK PARA A PAGINA DE BLOG -->
                    <a href="<?php echo get_permalink(61) ?>" class="articleBtnLink">
                        <div class="articleBtn">mais artigos</div>
                    </a>
                </div>
            </div>
        </section>



<?php
get_footer();