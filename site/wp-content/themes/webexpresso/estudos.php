<?php
/**
* Template Name: Estudos
*
* @package WebExpresso
* @subpackage Grano Studio
* @since Grano Studio 1.0
*/
get_header();
?>
<div class="breadcumbs">
        <div class="wrapperContent">
        <?php
                        if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb( '<p id="breadcumbsText">','</p>' );
                        }
                        ?>
        </div>
</div>

<section class="estudos top-section">
    <div class="wrapperContent flex-container">
        <div class="col-1">
            <!-- <p class="cat-text">Estudo</p> -->
            <h1 class="title-lg">Sobre  os conteúdos analíticos da Lozinsky</h1>
            <p>O entendimento dos fenômenos de mercado é parte crucial de um bom planejamento estratégico, da definição do plano de TI e da qualidade da tomada de decisão. Mas números sobre tendências e prioridades de investimento em tecnologia, quando desprovidos de contexto e de profundidade de análise, pouco podem dizer sobre as chances de sucesso de uma organização.</p>
            <p>Portanto, reunimos aqui alguns conteúdos exclusivos produzidos por nosso experiente time de consultores. Eles trazem às análises não só conhecimento teórico, mas principalmente uma visão estruturada sobre o que vivenciam no mercado, na imersão em dezenas de empresas atendidas pela Lozinsky.</p>
        </div>
        <div class="col-1 estudo-box">
            <p class="cat-text">Estudo</p>
            <img src="<?php echo get_template_directory_uri() ?>/images/atuacao/icon-estudo.png" alt="">
            <h3>Jornada do CIO: da realização pessoal ao sucesso do negócio</h3>
            <p>Nunca foi tão difícil trabalhar com TI. E nunca foi tão difícil ocupar a posição de CIO. Nesse contexto, o estudo “Jornada do CIO: da realização pessoal ao sucesso do negócio” busca mapear a trilha de aprendizagem e de crescimento dos líderes de TI nos dias de hoje, de forma a cruzar o perfil, as expectativas e os planos desses executivos com o caminho de sucesso (ou insucesso) das organizações.</p>
            <p>O resultado desse levantamento será um retrato inédito, profundo e realista desse profissional - quem ele é, onde está e o que realmente almeja? O objetivo é prover à alta liderança das empresas uma interpretação sobre possíveis âncoras para o crescimento, a partir da perspectiva da organização de TI.</p>
            <a href="https://forms.gle/ev59jHQ2ZvtujzEc6" class="btn-link">
                questionário da pesquisa 
            </a>
        </div>
    </div>
</section>
<section class="estudos bg-black">
    <h3 class="title-lg">Antes da TI, a Estratégia</h3>
    <div class="wrapperContent flex-container">
        <div class="col-1">
            <p>A Lozinsky é a consultoria parceria da IT Mídia na realização do estudo “Antes da TI, a Estratégia”, desde a validação do questionário de pesquisa até a análise dos resultados. O objetivo da pesquisa, que está em sua 9ª edição, é prover ao mercado conhecimento relevante para a tomada de decisão, estimulando uma reflexão sobre as demandas e as prioridades de investimentos em TI.</p>
        </div>
        <div class="col-1">
            <p>A pesquisa atinge os CIOs das mil maiores empresas do Brasil e busca, ainda, compreender o grau de maturidade dessas organizações em relação ao posicionamento da TI do negócio.
Veja dados exclusivos e análises da pesquisa:</p>
                        <img src="" alt="">
                        <img src="" alt="">
        </div>
    </div>
</section>
<section class="estudos artigos articleBlock">
        <div class="wrapperContent">
            <div class="articlePostBlock">
                <!-- QUERY DE POST -->
                <?php $post_args = array( 'post_type' => 'post', 'posts_per_page' => 3, 'author' => 9 );
                $post_query = new WP_Query( $post_args ); 
                ?>
                <?php if ( $post_query->have_posts() ) : ?>
                    <?php while ( $post_query->have_posts() ) : $post_query->the_post(); ?>
                        <div class="articlePostItem">
                        <!-- IMAGEM DESTACADA -->
                        <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>
                            <div class="articleItemImg" style="background-image: url(<?php echo $featured_img_url ?>)">
                        
                            <?php
                                //get all the categories the post belongs to
                                $categories = wp_get_post_categories( get_the_ID() );
                                //loop through them
                                foreach($categories as $c){
                                $cat = get_category( $c );
                                //get the name of the category
                                $cat_id = get_cat_ID( $cat->name );
                                //make a list item containing a link to the category
                                echo '<a href="'.get_category_link($cat_id).'"><p class="articleCatText">'.$cat->name.'</p></a>';
                                }
                            ?>
                            <?php
                            $author_id = get_the_author_meta( 'ID' );
                            $author_photo = get_field("foto",  'user_'.$author_id);?>
                            <div class="articleItemAuthor" style="background-image: url(<?php echo $author_photo ?>)"></div>
                        </div>
                        <div class="articleItemBlock">
                            <a href="<?php the_permalink(); ?>"><h3 class="articleItemTitle"> <?php echo get_the_title() ?></h3></a>
                            <?php $content = get_the_excerpt();?>
                            <!-- LIMITA O CONTENT EM 180 CARACTERES -->
                            <!-- <div class="articleItemText"><?php //echo mb_strimwidth($content, 0, 180, '...'); ?></div> -->
                            <a href="<?php the_permalink(); ?>" style="color: #353233;">
                                <div class="articleItemText"><?php echo wp_strip_all_tags($content);?></div>
                            </a>
                            <a href="<?php the_permalink(); ?>"><p class="articleItemReadMore">Leia o artigo</p></a>
                        </div>
                        <div class="articleItemFooter">
                            <p class="articleItemFooterText articleItemDate"><?php echo get_the_date(); ?></p>
                            <!-- <p class="articleItemFooterText articleItemCountComents">Nenhum comentário</p> -->
                        </div>
                     </div>
                     <?php endwhile; 
                        wp_reset_postdata();
                        else : ?>
                        <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
                    <?php endif; ?>
                
            </div>
            <!-- <div class="articleBlockButton"> -->
                <!-- LINK PARA A PAGINA DE BLOG -->
                <!-- <a href="<?php// echo get_permalink(61) ?>" class="articleBtnLink"> -->
                    <!-- <div class="articleBtn">mais artigos</div> -->
                <!-- </a> -->
            <!-- </div> -->
        </div>
</section>
<section class="estudos report" id="report">
    <div class="flex-container image">
        <div class="col-1">
            <img src="<?php echo get_template_directory_uri() ?>/images/atuacao/report-cover.png" alt="">
        </div>
        <div class="col-1"></div>
    </div>
    <div class="flex-container titulo">
        <div class="col-1 block-blue"></div>
        <div class="col-1">
            <p class="cat">Report</p>
            <h3 class="title-lg">Sistemas de gestão em Saúde: uma análise de mercado e tendências</h3>
        </div>
    </div>
    <div class="flex-container">
        <div class="col-1 "></div>
        <div class="col-1">
            <p>A criação de redes de informação e assistência impacta a estratégia de adoção de ERPs nas maiores instituições de Saúde do Brasil. O principal desafio é inaugurar uma nova fase na qual esses sistemas reúnam dados organizados, disponíveis, confiáveis e intercambiáveis - o que vai além do ERP. 
        </div>
    </div>
    <div class="conteudo-formulario" id="formBlock">
        <div class="wrapperContent flex-container">
            <div class="col-1">
                <p>Esse conteúdo teve como ponto de partida a segunda edição do relatório “ERPs nas mil maiores empresas brasileiras”, realizado por Leopoldo Barros, consultor associado da Lozinsky Consultoria. Na atualização do levantamento, em 2018, o especialista notou que o retrato da adoção de ERPs por essas organizações pouco havia mudado desde 2015, quando produziu a primeira edição do estudo, exceto por alguns poucos setores, em especial o de Saúde. Para aprofundar esse report, recorremos também à experiência do Aldir Rocha, sócio-consultor da Lozinsky Consultoria, que nos últimos anos vive uma intensa imersão nos projetos que desenvolvemos para instituições do setor.</p>
                <p>O resultado serve como um ponto de reflexão não só para as lideranças de TI, que eventualmente assumem uma responsabilidade maior do que a área pode (e deve) carregar, mas para todos os agentes e influenciadores da mudança nas instituições de Saúde.</p>
            </div>
            <div class="col-1">
                <h4>Faça download do report</h4>
                <?php
                    if(!isset($_POST['downloadNews'])) : //check if form was submitted
                ?>
                <?php else : ?>
                    <?php echo '<div class="positive-alert">Obrigado! Você receberá o link de Download por email.</div>'; ?>
                <?php endif;  ?>
                <form class="form-block" id="formEstudos" action="<?php echo get_permalink() ?>#formBlock" method="post">
                    <input class="contact-input" type="text" name="nome" id="" placeholder="Nome" required>
                    <input class="contact-input" type="text" name="sobrenome" id="" placeholder="Sobrenome" required>
                    <input class="contact-input" type="email" name="email" id="" placeholder="Email" required>
                    <select class="contact-input" id="cargo">
                        <option hidden disabled selected value >Cargo</option>
                        <option value="Analista de TI">Analista de TI</option>
                        <option value="CIO">CIO</option>
                        <option value="Supervisor de TI">Supervisor de TI</option>
                        <option value="Supervisor de área financeira">Supervisor de área financeira</option>
                        <option value="CFO">CFO</option>
                        <option value="CEO">CEO</option>
                        <option value="Outros">Outros</option>
                    </select>
                    <input class="contact-input" type="text" name="empresa" id="" placeholder="Empresa">
                    <select class="contact-input" id="faturamento">
                        <option hidden disabled selected value >Faixa de faturamento</option>
                        <option value="Até 250 milhões">Até 250 milhões</option>
                        <option value="Entre 251 milhões e 500 milhões">Entre 251 milhões e 500 milhões</option>
                        <option value="Entre 501 milhões e 1 bilhão">Entre 501 milhões e 1 bilhão</option>
                        <option value="Entre 1 bilhão e 2 bilhões">Entre 1 bilhão e 2 bilhões</option>
                        <option value="Entre 2 bilhões e 5 bilhões">Entre 2 bilhões e 5 bilhões</option>
                        <option value="Acima de 5 bilhões">Acima de 5 bilhões</option>
                        <option value="Não posso informar">Não posso informar</option>
                    </select>
                    <div class="switch-block">
                        <label class="switch">
                            <input type="checkbox" checked>
                            <span class="slider round"></span>
                        </label>
                        <p class="switch-text">Quero receber atualizações dos próximos reports</p>
                    </div>
                    <input type="submit" class="contact-submit" name="downloadNews" value="Download">
                    <!-- <button class="contact-submit" type="submit">Download</button> -->
                </form>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();
?>