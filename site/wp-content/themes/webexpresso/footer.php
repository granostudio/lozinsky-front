<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WebExpresso
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */
?>
<?php wp_footer(); ?>

<div class="formContact">
        <div class="wrapperContent">
            <a href="/contato">
                <div class="formContactContent">
                    <h3 class="formContactContentText">Entre em contato com nossos consultores</h3>
                </div>
            </a>
        </div>
        <a href="#bodyLozinsky" class="linkToTop">
            <div class="link-bar"></div>
            <p class="linkText">top</p>
        </a>
    </div>
    <footer>
        <div class="wrapperContent">
            <div class="menuListsBlock">
                <div class="footerMenuBlock">
                    <?php
                        wp_nav_menu(
                            array(
                                'theme_location' => 'primary',
                                'menu_class' => 'footerMenuBlockList',
                            )
                        );
					?>
                    <!-- <ul class="footerMenuBlockList">
                        <a href="#"><li class="footerMenuListItem">Home</li></a>
                        <a href="#"><li class="footerMenuListItem">Empresa</li></a>
                        <a href="#"><li class="footerMenuListItem">Cases</li></a>
                        <a href="#"><li class="footerMenuListItem">Reports</li></a>
                        <a href="#"><li class="footerMenuListItem">Areas de Atuação</li></a>
                        <a href="#"><li class="footerMenuListItem">Carreira</li></a>
                        <a href="#"><li class="footerMenuListItem">Contato</li></a>
                    </ul> -->
                </div>
                <div class="footerMenuBlock">
                    <ul class="footerMenuBlockList">
                    <?php 
                        $args = array(
                        'orderby' => 'name',
                        'hierarchical' => 1,
                        'taxonomy' => 'category',
                        'hide_empty' => 0,
                        'parent' => 0,
                        );
                        $categories = get_categories($args);

                        foreach($categories as $category) {
                            if($category->name != "Sem categoria"){
                                echo "<li class='menu-item'>";
                                echo '<a href="' . get_category_link($category->cat_ID) . '" title="' . $category->name . '">' . $category->name . '</a>';
                                echo "</li>";
                            }
                    
                        } 
                    ?>
                    </ul>
                </div>
            </div>
            <div class="footerNewsletter">
                <h3 class="newsTitle">Insights, reports e news.</h3>
                <div class="newsText">Seja assinante e</div>
                <div class="newsText">sempre receba novidades</div>
                <form class="newsForm" action="">
                    <input class="newsInput" type="email" placeholder="Email">
                    <button class="newsBtn">assinar</button>
                </form>
            </div>
        </div>
    </footer>


<!-- Get Scripts -->
<?php 
$scripts = get_field('adicionar_script', 'option');
if($scripts)
{
	foreach($scripts as $script)
    {  
        if($script['posicao']=="Footer"){
            echo $script['script'];
        } 
	}
}   
?>

<!-- Google Analytics -->
<?php 
// $seoanalytics = grano_get_options('seo','seo_analytics');

// if(!empty($seoanalytics) || $seoanalytics != "Default Text"){
//   echo "<script>";
//   echo $seoanalytics;
//   echo "</script>";
// }

 ?>
<!-- / Google Analytics -->

</body>

</html>
