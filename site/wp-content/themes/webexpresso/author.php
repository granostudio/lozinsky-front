<?php  get_header(); ?>



<!-- This sets the $curauth variable -->

    <?php
    $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
    ?>

            <?php 
                if(($curauth->first_name !== '') && ($curauth->last_name !== '') ) {
                    $showName = $curauth->first_name . ' ' . $curauth->last_name;
                }
                else {
                    $showName = $curauth->nickname;
                }
            ?>


    <div class="breadcumbs">
        <div class="wrapperContent">
            <p id="breadcumbsText">
                Autor: <?php echo $showName; ?>
            </p>
        </div>
    </div>

    <div id="content" class="narrowcolumn">

    <section class="articleBlock">
        <div class="wrapperContent">
            <h3 class="articleTitle">Artigos de <?php echo $showName; ?></h3>
        </div>
        <div class="wrapperContent">
            <div class="articlePostBlock">

            <?php 
                $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                $post_args = array( 'post_type' => 'post', 'posts_per_page' => 6, 'paged' => $paged, 'orderby' => 'date', 'author' => $curauth->ID  );
                $post_query = new WP_Query( $post_args ); 
            ?>

    <?php if ( $post_query->have_posts() ) : while ( $post_query->have_posts() ) : $post_query->the_post(); ?>
    <div class="articlePostItem">
                        <!-- IMAGEM DESTACADA -->
                        <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>
                            <div class="articleItemImg" style="background-image: url(<?php echo $featured_img_url ?>)">
                        
                            <?php
                                //get all the categories the post belongs to
                                $categories = wp_get_post_categories( get_the_ID() );
                                //loop through them
                                foreach($categories as $c){
                                $cat = get_category( $c );
                                //get the name of the category
                                $cat_id = get_cat_ID( $cat->name );
                                //make a list item containing a link to the category
                                echo '<a href="'.get_category_link($cat_id).'"><p class="articleCatText">'.$cat->name.'</p></a>';
                                }
                            ?>
                            <?php
                            $author_id = get_the_author_meta( 'ID' );
                            // $author_photo = get_field("foto",  'user_'.$author_id);?>
                            <!-- <div class="articleItemAuthor" style="background-image: url(<?php //echo $author_photo ?>)"></div> -->
                        </div>
                        <div class="articleItemBlock">
                            <a href="<?php the_permalink(); ?>"><h3 class="articleItemTitle"> <?php echo get_the_title() ?></h3></a>
                            <?php $content = get_the_excerpt();?>
                            <!-- LIMITA O CONTENT EM 180 CARACTERES -->
                            <!-- <div class="articleItemText"><?php //echo mb_strimwidth($content, 0, 180, '...'); ?></div> -->
                            <div class="articleItemText"><?php echo wp_strip_all_tags($content);?></div>
                            <a href="<?php the_permalink(); ?>"><p class="articleItemReadMore">Leia o artigo</p></a>
                        </div>
                        <div class="articleItemFooter">
                            <p class="articleItemFooterText articleItemDate"><?php echo get_the_date(); ?></p>
                            <!-- <p class="articleItemFooterText articleItemCountComents">Nenhum comentário</p> -->
                        </div>
                     </div>

            <?php endwhile; else: ?>
                <p><?php _e('No posts by this author.'); ?></p>

            <?php endif; ?>

        </div>
        <!-- End Loop -->

        <div class="articleBlockButton">
            <div href="#" class="articleBtnLink">
                <div class="articleBtn">
                    <?php
                    echo paginate_links( array(
                        'format'          => 'page/%#%',
                        'current'         => $paged,
                        'total'           => $post_query->max_num_pages,
                        'mid_size'        => 2,
                        'prev_text'       => __('<'),
                        'next_text'       => __('>')
                    ) );
                    ?>
                </div>
            </div>
        </div>
    </section>

    

</div>
<?php get_footer(); ?>