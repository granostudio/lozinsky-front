<?php
/**
 * 
 * 
 * 
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */

get_header(); ?>

<div class="breadcumbs">
    <div class="wrapperContent">
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb( '<p id="breadcumbsText">','</p>' );
        }
        ?>
    </div>
</div>
<!-- CONTEUDO DO POST -->
<section class="main-post" id="main-post">
            <?php
			/* Start the Loop */
			while ( have_posts() ) :
                the_post();
                
                #vars
                $categories = get_the_category();
                $title = get_the_title();
                // $content = the_content();
                $autor = get_the_author();
                $autor_photo = get_field("foto",  'user_'.get_the_author_meta('ID'));
                $autor_occupation = get_field("occupation",  'user_'.get_the_author_meta('ID'));
                $autor_description = get_the_author_meta('description');
                $autor_linkedin = get_field("linkedin",  'user_'.get_the_author_meta('ID'));
                #thumbanail
                $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
                // link tp share
                $actual_link = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                $actual_link_postID = get_the_ID();

                ?>
        <div class="wrapperContent">
            <div class="cat-title-block">
                <a href=""><h2 class="cat-title cat-line"><?php echo esc_html( $categories[0]->name ); ?></h2></a> 
            </div>
            <div class="date-top-block" style="display: none;">
                <p class="date-text"><?php echo get_the_date(); ?></p>
            </div>
            <div class="main-post-info">
                <!-- CONTEUDO POST -->
                <div class="main-post-content-block">
                    <div class="main-post-img" style="background-image: url(<?php echo $featured_img_url; ?>)">
                        <h1 class="main-post-title"><?php echo esc_html($title); ?></h1>
                    </div>
                    <?php if( get_field('arquivo_de_podcast') ): ?>
                        <div class="podcast-block" style="display: none;">
                            <div class="podcast-player-block">
                                <?php the_field('arquivo_de_podcast'); ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="main-post-text-content">
                        <?php the_content(); ?>
                    </div>
                </div>
                <!-- INFO USER -->
                <div class="main-post-user-block">
                    <h2 class="post-user-title">artigo assinado por</h2>
                    <div class="main-post-user-block-content">
                        <div class="user-img" style="background-image: url(<?php echo $autor_photo; ?>)"></div>
                        <p class="user-name"><?php echo $autor; ?></p>
                        <p class="user-text"><?php echo $autor_occupation; ?></p>
                        <div class="user-block-content">
                            <p class="user-text"><?php echo $autor_description; ?></p>
                            <a href="<?php echo get_author_posts_url( get_the_author_meta('ID') ); ?>" class="link-default">Veja mais artigos do consultor</a>
                            <div class="user-block-content-footer">
                                <!-- <a href="" class="see-more-link"><p class="see-more-text">leia mais artigos
                                        deste autor</p></a> -->
                                <a href="<?php echo $autor_linkedin; ?>" class="social-link" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="share-bottom">
                        <p class="post-user-title">compartilhe este artigo</p>
                        <ul class="social-list">
                            <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $actual_link;?>&title=<?php echo $title;?>&summary=&source=" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                            <a href="https://www.facebook.com/sharer/sharer.php?u=http://<?php echo $actual_link;?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
                            <a href="https://twitter.com/intent/tweet?url=http://<?php echo $actual_link;?>" target="_blank"><i class="fab fa-twitter"></i></a>
                        </ul>
                    </div>
                    <?php include 'components/atuacao-sidebar-block.php'; ?>
                    <?php include 'components/report-sidebar-block.php'; ?>
                </div>
            </div>
        </div>
        <?php

			endwhile; // End of the loop.
			?>
    </section>
    <div class="control-post">
        <div class="wrapperContent">
            <div class="control-post-content">
                <?php previous_post_link( '%link', 'artigo anterior' ); ?>
                <?php next_post_link( '%link', 'próximo artigo' ); ?>
            </div>
        </div>
    </div>

    <section class="articleBlock">
            <div class="wrapperContent">
                <h3 class="articleTitle">outros artigos</h3>
            </div>
            <div class="wrapperContent">
            <div class="articlePostBlock">
                <!-- QUERY DE POST -->
                <?php $post_args = array( 'post_type' => 'post', 'posts_per_page' => 3, 'post__not_in' => array($actual_link_postID));
                $post_query = new WP_Query( $post_args ); 
                ?>
                <?php if ( $post_query->have_posts() ) : ?>
                    <?php while ( $post_query->have_posts() ) : $post_query->the_post(); 
                        
                    ?>
                        <div class="articlePostItem">
                        <!-- IMAGEM DESTACADA -->
                        <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>
                            <div class="articleItemImg" style="background-image: url(<?php echo $featured_img_url ?>)">
                        
                            <?php
                                //get all the categories the post belongs to
                                $categories = wp_get_post_categories( get_the_ID() );
                                //loop through them
                                foreach($categories as $c){
                                $cat = get_category( $c );
                                //get the name of the category
                                $cat_id = get_cat_ID( $cat->name );
                                //make a list item containing a link to the category
                                echo '<a href="'.get_category_link($cat_id).'"><p class="articleCatText">'.$cat->name.'</p></a>';
                                }
                            ?>
                            <?php
                            $author_id = get_the_author_meta( 'ID' );
                            $author_photo = get_field("foto",  'user_'.$author_id);?>
                            <div class="articleItemAuthor" style="background-image: url(<?php echo $author_photo ?>)"></div>
                        </div>
                        <div class="articleItemBlock">
                            <a href="<?php the_permalink(); ?>"><h3 class="articleItemTitle"> <?php echo get_the_title() ?></h3></a>
                            <?php $content = get_the_excerpt();?>
                            <!-- LIMITA O CONTENT EM 180 CARACTERES -->
                            <a href="<?php the_permalink(); ?>" style="color: #353233;">
                                <div class="articleItemText"><?php echo wp_strip_all_tags($content) ?></div>
                            </a>
                            <!-- <a href="<?php // the_permalink(); ?>"><p class="articleItemReadMore">Leia o artigo</p></a> -->
                            <a href="<?php the_permalink(); ?>"><p class="articleItemReadMore"><?php the_field('texto_do_link'); ?></p></a>
                        </div>
                        <div class="articleItemFooter">
                            <p class="articleItemFooterText articleItemDate"><?php echo get_the_date(); ?></p>
                            <!-- <p class="articleItemFooterText articleItemCountComents">Nenhum comentário</p> -->
                        </div>
                     </div>
                     <?php 
                        endwhile; 
                        wp_reset_postdata();
                        else : ?>
                        <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
                    <?php endif; ?>
                
                </div>
                <div class="articleBlockButton">
                    <!-- LINK PARA A PAGINA DE BLOG -->
                    <a href="<?php echo get_permalink(61) ?>" class="articleBtnLink">
                        <div class="articleBtn">mais artigos</div>
                    </a>
                </div>
            </div>
        </section>



<?php
get_footer();
