<?php
/**
* Pagina do blog
*
* @package WebExpresso
* @subpackage Grano Studio
* @since Grano Studio 1.0
*/
get_header();

if ( !is_paged() ) :
    // first page of pagination
?>

<!-- ################# SLIDER TOP BLOCK ################# -->
    <div class="sliderTop" id="sliderTop">
        <a href="#spotlight" class="linkToDown">
            <div class="link-bar"></div>
            <p class="linkText">scroll</p>
        </a>
        <div class="sliderTopContent">
            <?php
                if( have_rows('banner_blog', 'option') ):

                    // loop through the rows of data
                   while ( have_rows('banner_blog', 'option') ) : the_row();
               
                    // vars
                    $imagem = get_sub_field('imagem');
                    $categoria = get_sub_field('categoria');
                    $titulo = get_sub_field('titulo');
                    $titulo_2 = get_sub_field('titulo_2');
                    $estilo_do_botao = get_sub_field('estilo_do_botao');
                    $link = get_sub_field('link');

                    ?>
                    <div class="sliderTopContentItem" style="background-image: url(<?php echo $imagem ?>)">
                        <div class="wrapperContent">
                            <div class="contentItemInfo lightInfo">
                                <p class="catItem"><?php echo $categoria ?></p>
                                <h2 class="titleItem"><?php echo $titulo ?></h2>
                                <p class="textItem"><?php echo $titulo_2 ?></p>
                                <?php
                                    if($estilo_do_botao == "Assista"){
                                       ?>
                                        <a href="<?php echo $link ?>" class="linkVideo">
                                            <svg version="1.0" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                            width="109.279px" height="36.542px" viewBox="0 0 109.279 36.542" enable-background="new 0 0 109.279 36.542"
                                            xml:space="preserve">
                                            <g id="guides">
                                            </g>
                                            <g id="lg-site">
                                                <g>
                                                    <g>
                                                        <defs>
                                                            <rect id="SVGID_46_" x="-181.887" y="245.267" width="1200" height="541.402"/>
                                                        </defs>
                                                        <clipPath id="SVGID_2_">
                                                            <use xlink:href="#SVGID_46_"  overflow="visible"/>
                                                        </clipPath>
                                                        <g id="JvdQb4.tif_2_" clip-path="url(#SVGID_2_)">
                                                            
                                                                <image overflow="visible" width="1200" height="786" id="Camada_1_9_" xlink:href="C339896EE48E83E4.png"  transform="matrix(1.0008 0 0 1.0008 -182.3496 11.6689)">
                                                            </image>
                                                        </g>
                                                    </g>
                                                    <g>
                                                        <path fill="none" class="play-button-svg" stroke="#FFFFFF" stroke-miterlimit="10" d="M104.376,6.59h-67.48c-2.99,0-5.857-1.028-8.287-2.771
                                                            C25.546,1.623,21.757,0.375,17.67,0.51C8.611,0.81,1.107,8.073,0.536,17.119c-0.652,10.337,7.539,18.924,17.735,18.924
                                                            c3.542,0,6.841-1.038,9.611-2.826c2.137-1.378,4.661-2.033,7.203-2.033h69.29c2.422,0,4.403-1.981,4.403-4.403V10.993
                                                            C108.779,8.571,106.798,6.59,104.376,6.59z"/>
                                                        <path fill="#FFFFFF" class="play-icon-svg" d="M12.851,27.947V9.827c0-1.395,1.604-2.179,2.705-1.322l11.642,9.06c0.862,0.671,0.862,1.975,0,2.646
                                                            l-11.642,9.06C14.455,30.125,12.851,29.341,12.851,27.947z"/>
                                                        
                                                            <text transform="matrix(1 0 0 1 38.0889 23.3154)" fill="#FFFFFF" font-family="'Montserrat-Light'" font-size="15.2944" letter-spacing="1">assista</text>
                                                    </g>
                                                </g>
                                            </g>
                                            </svg>
                                            
                                        </a>
                                       <?php
                                    } else {
                                        ?>
                                        <a href="<?php echo $link ?>" class="linkReadMore">
                                            <svg version="1.0" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                width="134.279px" height="36.542px" viewBox="0 0 134.279 36.542" enable-background="new 0 0 134.279 36.542"
                                                xml:space="preserve">
                                                <g id="guides">
                                                </g>
                                                <g id="lg-site">
                                                    <g>
                                                        <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M129.376,6.589h-92.48c-2.99,0-5.857-1.028-8.287-2.771
                                                            C25.546,1.622,21.757,0.375,17.67,0.51C8.611,0.809,1.107,8.073,0.536,17.118c-0.651,10.337,7.538,18.924,17.735,18.924
                                                            c3.542,0,6.841-1.039,9.611-2.826c2.137-1.378,4.66-2.033,7.203-2.033h94.29c2.422,0,4.403-1.981,4.403-4.403V10.993
                                                            C133.779,8.571,131.798,6.589,129.376,6.589z"/>
                                                        
                                                            <text transform="matrix(1 0 0 1 40.0889 23.3154)" fill="#FFFFFF" font-family="'Montserrat-Light'" font-size="15.2944" letter-spacing="1">leia o artigo</text>
                                                        
                                                            <line fill="none" stroke="#FFFFFF" stroke-linecap="round" stroke-miterlimit="10" x1="14.407" y1="11.242" x2="27.885" y2="11.242"/>
                                                        
                                                            <line fill="none" stroke="#FFFFFF" stroke-linecap="round" stroke-miterlimit="10" x1="17.271" y1="14.517" x2="27.885" y2="14.517"/>
                                                        
                                                            <line fill="none" stroke="#FFFFFF" stroke-linecap="round" stroke-miterlimit="10" x1="21.146" y1="17.766" x2="27.885" y2="17.766"/>
                                                        
                                                            <line fill="none" stroke="#FFFFFF" stroke-linecap="round" stroke-miterlimit="10" x1="21.146" y1="21.19" x2="27.885" y2="21.19"/>
                                                        
                                                            <line fill="none" stroke="#FFFFFF" stroke-linecap="round" stroke-miterlimit="10" x1="17.271" y1="24.108" x2="27.885" y2="24.108"/>
                                                        
                                                            <line fill="none" stroke="#FFFFFF" stroke-linecap="round" stroke-miterlimit="10" x1="14.407" y1="27.532" x2="27.885" y2="27.532"/>
                                                        
                                                            <line fill="none" stroke="#FFFFFF" stroke-linecap="round" stroke-miterlimit="10" x1="5.938" y1="19.616" x2="15.438" y2="19.616"/>
                                                        
                                                            <line fill="none" stroke="#FFFFFF" stroke-linecap="round" stroke-miterlimit="10" x1="12.521" y1="16.713" x2="15.438" y2="19.634"/>
                                                        
                                                            <line fill="none" stroke="#FFFFFF" stroke-linecap="round" stroke-miterlimit="10" x1="12.354" y1="22.243" x2="15.438" y2="19.634"/>
                                                    </g>
                                                </g>
                                            </svg>                           
                                        </a>
                                        <?php
                                    }
                                ?>
                                
                            </div>
                        </div>
                    </div>
                    <?php

                   endwhile;
                endif;

            ?>
        </div>
        <div class="sliderTopControl">
            <div class="arrowBlock"></div>
            <div class="sliderTopInfo"></div>
        </div>
    </div>
    <!-- ############# END SLIDER TOP BLOCK ############ -->

    <section class="spotlight" id="spotlight">
        <div class="wrapperContent">
            <div class="post-highlighted-lg">
            <?php
                $post_id_destaque1 = get_field('destaques_blog', 'option')['destaque_1'];
                if($post_id_destaque1):
                    $queried_post = get_post($post_id_destaque1);
                    
                    // vars
                    $data      = get_the_date("", $post_id_destaque1);
                    $title     = $queried_post->post_title;
                    $content   = $queried_post->post_content;
                    $link      = $queried_post->guid;
                    $author_id = $queried_post->post_author;
                    $author_photo = get_field('foto', 'user_'.$author_id);
                    /* grab the url for the full size featured image */
                    $featured_img_url = get_the_post_thumbnail_url($post_id_destaque1,'full');
                endif;
            ?>
                <div class="data-post-block">
                    <p class="data-text"><?php echo $data ?></p>
                    <!-- <p class="data-text">Nenhum comentário</p> -->
                </div>
                <div class="post-content">
                    <h3 class="post-title"><a href="<?php echo $link ?>"> <?php echo $title ?></a></h3>
                    <div class="post-text"><?php echo mb_strimwidth($content, 0, 700, '...');?></div>
                </div>
                <a href="<?php echo $link ?>">
                    <div class="post-img" style="background-image: url(<?php echo $featured_img_url ?>)"></div>
                </a>
                <a href="<?php echo get_author_posts_url( $author_id ); ?>" style="position: relative;">
                    <div class="post-user" style="background-image: url(<?php echo $author_photo ?>)"></div>
                </a>
            </div>
            <?php
                $post_id_destaque2 = get_field('destaques_blog', 'option')['destaque_2'];
                if($post_id_destaque2):
                    $queried_post = get_post($post_id_destaque2);
                    
                    // vars
                    $data      = get_the_date("", $post_id_destaque2);
                    $title     = $queried_post->post_title;
                    $content   = $queried_post->post_content;
                    $link      = $queried_post->guid;
                    $author_id = $queried_post->post_author;
                    $author_photo = get_field('foto', 'user_'.$author_id);
                    $categories = get_the_terms( $post_id_destaque2, 'category' );
                    /* grab the url for the full size featured image */
                    $featured_img_url = get_the_post_thumbnail_url($post_id_destaque2,'full');
                endif;
            ?>
            <div class="post-highlighted-sm">
                <div class="post-sm-bg" style="background-image: url(<?php echo $featured_img_url ?>)">
                    <div class="post-cat-block">
                        <a href="#">
                            <div class="post-cat-item"><?php echo $categories[0]->name; ?></div>
                        </a>
                    </div>
                    <h2 class="post-title"><a href="<?php echo $link ?>"> <?php echo $title ?></a></h2>
                    <a href="<?php echo get_author_posts_url( $author_id ); ?>">
                        <div class="post-user" style="background-image: url(<?php echo $author_photo ?>)"></div>
                    </a>
                </div>
                <div class="post-sm-content">
                    <div class="post-content-block">
                        <div class="post-text"><?php echo mb_strimwidth($content, 0, 100, '...');?></div>
                        <!-- <a href="<?php //echo $link ?>"><p class="post-link">Leia o artigo</p></a> -->
                        <a href="<?php echo $link ?>"><p class="post-link"><?php the_field('texto_do_link'); ?></p></a>
                    </div>
                    <div class="post-content-footer">
                        <p class="post-text"><?php echo $data;?></p>
                        <!-- <p class="post-text">Nenhum comentário</p> -->
                    </div>
                </div>
            </div>
        </div>
    </section>
            <?php else :
                ?>
                <div class="breadcumbs">
                    <div class="wrapperContent">
                        <?php
                        if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb( '<p id="breadcumbsText">','</p>' );
                        }
                        ?>
                    </div>
                </div>
                <?php
            endif;?>
    <section class="articleBlock">
        <div class="wrapperContent">
            <div class="menuList">
                <div class="menu-article">
                    <a href="/blog-home" class="active">todos</a>
                    <?php 
                        $args = array(
                        'orderby' => 'name',
                        'hierarchical' => 1,
                        'taxonomy' => 'category',
                        'hide_empty' => 0,
                        'parent' => 0,
                        );
                        $categories = get_categories($args);

                        foreach($categories as $category) {
                            if($category->name != "Sem categoria"){
                                echo '<a href="' . get_category_link($category->cat_ID) . '" title="' . $category->name . '">' . $category->name . '</a><br>';
                            }
                    
                        } 
                    ?>
                </div>
                <div class="searchMenuBlock">
                    <button class="trigger">
                        <img src="<?php echo get_template_directory_uri() ?>/images/lupa.png" alt="">
                    </button>
                </div>
            </div>
          
            <div class="modalSeacrh">
                <div class="modal-content">
                    <span class="close-button">×</span>
                    <div class="modal-form">
                        <?php get_search_form(); ?>
                    </div>
                </div>
            </div>

            <div class="articlePostBlock">
                <!-- QUERY DE POST -->
                
                <?php 
                $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                $post_args = array( 'post_type' => 'post', 'posts_per_page' => 6, 'paged' => $paged, 'orderby' => 'date' );
                $post_query = new WP_Query( $post_args ); 
                ?>
                <?php if ( $post_query->have_posts() ) : ?>
                    <?php while ( $post_query->have_posts() ) : $post_query->the_post(); ?>
                        <div class="articlePostItem">
                        <!-- IMAGEM DESTACADA -->
                        <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>
                            <div class="articleItemImg" style="background-image: url(<?php echo $featured_img_url ?>)">
                        
                            <?php
                                //get all the categories the post belongs to
                                $categories = wp_get_post_categories( get_the_ID() );
                                //loop through them
                                foreach($categories as $c){
                                $cat = get_category( $c );
                                //get the name of the category
                                $cat_id = get_cat_ID( $cat->name );
                                //make a list item containing a link to the category
                                echo '<a href="'.get_category_link($cat_id).'"><p class="articleCatText">'.$cat->name.'</p></a>';
                                }
                            ?>
                            <?php
                            $author_id = get_the_author_meta( 'ID' );
                            $author_photo = get_field("foto",  'user_'.$author_id);?>
                            <p><?php $author_photo ?></p>
                                <!-- <div class="articleItemAuthor" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/author-img.jpg)"></div> -->
                            <a href="<?php echo get_author_posts_url( $author_id ); ?>">
                                <div class="articleItemAuthor" style="background-image: url(<?php echo $author_photo ?>)"></div>
                            </a>
                        </div>
                        <div class="articleItemBlock">
                            <a href="<?php the_permalink(); ?>"><h3 class="articleItemTitle"> <?php echo get_the_title() ?></h3></a>
                            <?php $content = get_the_excerpt();?>
                            <!-- LIMITA O CONTENT EM 180 CARACTERES -->
                            <a href="<?php the_permalink(); ?>" style="color: #353233;"><div class="articleItemText"><?php echo wp_strip_all_tags($content);?></div></a>
                            <!-- <a href="<?php //the_permalink(); ?>"><p class="articleItemReadMore">Leia o artigo</p></a> -->
                            <a href="<?php the_permalink(); ?>"><p class="articleItemReadMore"><?php the_field('texto_do_link'); ?></p></a>
                        </div>
                        <div class="articleItemFooter">
                            <p class="articleItemFooterText articleItemDate"><?php echo get_the_date(); ?></p>
                            <!-- <p class="articleItemFooterText articleItemCountComents">Nenhum comentário</p> -->
                        </div>
                     </div>
                     <?php endwhile; 
                        wp_reset_postdata();
                        else : ?>
                        <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
                    <?php endif; ?>
                
            </div>
            <div class="articleBlockButton">
                <div href="#" class="articleBtnLink">
                    <div class="articleBtn">
                        <?php
                        echo paginate_links( array(
                            'format'          => 'page/%#%',
                            'current'         => $paged,
                            'total'           => $post_query->max_num_pages,
                            'mid_size'        => 2,
                            'prev_text'       => __('<'),
                            'next_text'       => __('>')
                        ) );
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();
?>

